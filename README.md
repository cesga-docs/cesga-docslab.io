![Build Status](https://gitlab.com/pages/sphinx/badges/master/pipeline.svg)

---

CESGA [sphinx] documentation website using GitLab Pages.

Learn more about GitLab Pages at https://about.gitlab.com/product/pages/ and the official
documentation https://docs.gitlab.com/ee/user/project/pages/.

---

# CESGA Documentation
This repository contains documentation about the different platforms available at [CESGA](https://www.cesga.es).


## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: python:3.7-alpine

test:
  stage: test
  script:
  - pip install -U sphinx
  - pip install -U sphinx_rtd_theme
  - pip install -U sphinx-material
  - pip install -U myst-parser
  - sphinx-build -b html . public
  rules:
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH

pages:
  stage: deploy
  script:
  - pip install -U sphinx
  - pip install -U sphinx_rtd_theme
  - pip install -U sphinx-material
  - pip install -U myst-parser
  - sphinx-build -b html . public
  artifacts:
    paths:
    - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

## Requirements

- [Sphinx][]

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][sphinx] Sphinx
    ```
    pip install -U sphinx
    pip install -U myst-parser
    ```
1. Install additional themes
    ```
    pip install -U sphinx_rtd_theme
    pip install -U sphinx-material
    ```
1. Generate the documentation: `make`


The generated HTML will be located in the location specified by `conf.py`,
in this case `_build/html`.

If you want to convert from rst to md you can use the `rst2myst` utility included in the following package:

    pip install -U "rst-to-myst[sphinx]"

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

No issues reported yet.

---

Forked from https://gitlab.com/Eothred/sphinx

[ci]: https://about.gitlab.com/product/continuous-integration/
[userpages]: https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#user-and-group-website-examples
[projpages]: https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#project-website-examples
[sphinx]: http://www.sphinx-doc.org/
