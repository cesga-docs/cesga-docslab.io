(aspera)=

% :title: Aspera
% :section: dtn-user-guide
% :url_source: https://cesga-docs.gitlab.io/dtn-user-guide/aspera.html
% :topic: aspera
% :keywords: ASPERA, PROPRIETARY SOFTWARE, LARGE FILES, WAN TRANSFERS, REDIRIS, GLOBUS, DTN, ASPERA CLI, SSH, DTN.SRV.CESGA.ES, FASPEX, USER, PASSWORD, EMAIL, DOWNLOAD, DOCUMENTATION.
% :content:

# Aspera

Aspera is a proprietary software specialized in sending and sharing large files. It uses a patented transport technology specially suited for WAN transfers.

The Aspera service is provided by RedIris and you need to contact us in order to use the service.

:::{note}
In most cases Globus is our preferred solution.
:::

## Installation

The DTN has the Aspera CLI already installed.

## Usage

Once you connect using SSH to dtn.srv.cesga.es you can use the Aspera CLI.

These are some of the most common commands:

- List the contents of a user's inbox, sent or archived messages:

  ```
  aspera faspex list -H faspex.rediris.es -u <user> -p <password>
  ```

- Send a package to a user or group:

  ```
  aspera faspex send --file ./my_large_file.img -H faspex.rediris.es  --title "Large file" --note "I am sending you a large file" --recipient <email> -u <user> -p <password>
  ```

- Get (download) a package:

  ```
  aspera faspex get -u <user> -p <password> --url "faspe://..."
  ```

For more information you can check the [Aspera CLI Documentation].

[aspera cli documentation]: http://download.asperasoft.com/download/docs/cli/3.7.7/user_linux/webhelp/index.html
