(known_issues)=

% :title: Known Issues
% :section: dtn-user-guide
% :url_source: https://cesga-docs.gitlab.io/dtn-user-guide/known_issues.html
% :topic: known_issues
% :keywords: TCLLIB, ANACONDA, GLOBUS CONNECT PERSONAL, GUI, PATH, BUG, ERROR, TCL VERSION
% :content:

# Known Issues

## tcllib error when using Anaconda

There is a bug in the tcl version distributed with some Anaconda versions that leads to an error when trying to start the Globus Connect Personal GUI.

The easiest solution is to change the PATH so it does not include the Anaconda directory.
