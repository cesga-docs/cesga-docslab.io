(dtn_want_to_know_more)=

% :title: Want to know more
% :section: dtn-user-guide
% :url_source: https://cesga-docs.gitlab.io/dtn-user-guide/want_to_know_more.html
% :topic: want_to_know_more
% :keywords: GLOBUS, ASPERA, CLI, DOCUMENTATION, HOWTOS
% :content:

# Want to know more

For more information you can look at the official documentation of each tool.

- [Globus Howtos]
- [Globus Documentation]
- [Aspera CLI Documentation].

[aspera cli documentation]: http://download.asperasoft.com/download/docs/cli/3.7.7/user_linux/webhelp/index.html
[globus documentation]: https://docs.globus.org/
[globus howtos]: https://docs.globus.org/how-to/
