(scp)=

% :title: SCP/SFTP
% :section: dtn-user-guide
% :url_source: https://cesga-docs.gitlab.io/dtn-user-guide/scp.html
% :topic: scp
% :keywords: SCP, SFTP, GLOBUS, DTN, CESGA, FINISTERRAE-III, RSYNC, SFTP, WINSCP, FILEZILLA, ASPERA, VPN, IP, LINUX, MAC, WINDOWS, SCP/SFTP CLIENT.
% :content:

# SCP/SFTP

Using SCP or SFTP is a useful alternative for small transfers (less than 10GB of data). Just keep in mind that you will get poorer performance than with Globus and if the connection is not stable you can find issues.

## Usage

**These nodes are exclusively for data transfer.** Any type of interaction not related to data transferring will be eliminated and the user could be penalized. There are two ways of accessing them based on being a CESGA user or not.

- **Access to the DTN nodes for CESGA users:**

Access to these nodes is equal to FinisTerrae-III login nodes. DTN nodes can be accessed by the hostname `dtn.srv.cesga.es`
User and password are the same for FinisTerrae III. You can use `rsync` or transfer commands to start the data transfer and they can also be directly accessed via sftp with the following command: `sftp username@dtn.srv.cesga.es`

There is also the possibility to use WinSCP, Filezilla or similar clients to perform the data tranfer using the following configuration:

```console
Hostname: dtn.srv.cesga.es
Port: 22
Username: username
Password: your_password
```

Also, [ASPERA](https://cesga-docs.gitlab.io/dtn-user-guide/aspera.html) is available in these nodes for those users who want or can use it as another way to perform the transfer.

:::{Warning}
VPN access to these nodes is NOT POSSIBLE, so data transfers must be made from the authorized work center of the user. Exceptionally, if a user is outside these centers, we can add the public IP to give them access. If this is your case, you must contact the Systems Department indicating the data transfer you want to perform, the public IP from where the connection will be made and the time duration (approximate) in order to grant you access to that IP .
:::

- **Access to the DTN nodes for external users:**

We refer to external users to those who are not registered in our services. The transfer of data between CESGA users and external is possible, also for uploading and downloading data from both parts. A possible scenario for this exchange would be sharing data with external collaborators of our users. If this is your case, the protocol to grant this access is:

By the CESGA user, contact the Systems Department, explaining that you need to transfer data (or receive data) from an external collaborator. If the transfer is from CESGA user to the external one, it must be indicated the directory where the data is located. The external user will have the credentials to access the DTN nodes through user + pass. In this case, the connection is only allowed by `sftp` command.

You must also provide the public IP of the external user to grant the access (because as previously mentioned, these nodes cannot be accessed via VPN and the IPs must be included in our system). You must also indicate the approximate time in which the service will be offered for the data transferring to be performed.

:::{Note}
You can also use a local SCP/SFTP client, this is usually already available in Linux and Mac. In case of Windows you can install for example [WinSCP] and use the host configuration mentioned above.
:::

[winscp]: https://winscp.net
