(cloud_openstack_glossary)=

% :title: Glossary
% :section: cloud-openstack
% :url_source: https://cesga-docs.gitlab.io/cloud-openstack/glossary.html
% :topic: glossary
% :keywords: CLOUD, OPENSTACK, FLOATING IP ADDRESS, INSTANCE, IMAGE, KEY PAIR, NETWORK, PORT, PROJECT, ROUTER, SECURITY GROUP, SNAPSHOT, VOLUME, VM, DNS, SSH, VIFS, VNICS, ISCSI, QUOTA, STORAGE, LAYER-2 NETWORK.
% :content:

# Glossary

Brief list of terms used in OpenStack to help you understand what they mean in CLOUD context:

**Floating IP address:** An IP address that a project can associate with a VM so that the instance has the same public IP address each time that it boots. You create a pool of floating IP addresses and assign them to instances as they are launched to maintain a consistent IP address for maintaining DNS assignment.

**Instance:** refers to the VM itself.

**Image:** One of several operating systems offered on the OpenStack platform. Users can choose what type of operating system wants for the VM.

**Key pair:** those are the public/private key pair associated for an user. These keys will allow the connection via SSH to the VM.

**Network:** A virtual network that provides connectivity between entities. For example, a collection of virtual ports that share network connectivity. In Networking terminology, a network is always a layer-2 network.

**Port:** A virtual network port within Networking; VIFs / vNICs are connected to a port.

**Project:** refers to the partition of Cloud resources to which a user has access. These resources constitute the total quota available to a user. It is important to differentiate between project and VM. A user only has one associated project within which they can deploy several VM. The amount of VMs that can be launched within a project is given by the amount of resources that each machine consumes within the project's quota. As previously mentioned, the project quota can be increased.

**Router:** virtual network device that passes network traffic between different networks.

**Security group:** A set of network traffic filtering rules that are applied to VM.

**Snapshot:** A point-in-time copy of an OpenStack storage volume or image. Use storage volume snapshots to back up volumes. Use image snapshots to back up data, or as “gold” images for additional servers.

**Volume:** Disk-based data storage generally represented as an iSCSI target with a file system that supports extended attributes; can be persistent or ephemeral. By default, the VMs have 1 associated volume of 40GB where the OS is allocated and one or more volumes where the data is stored.
