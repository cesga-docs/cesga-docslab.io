% CESGA CLOUD-Openstack User Guide documentation master file

% :title: CLOUD User Guide
% :section: cloud-openstack
% :url_source: https://cesga-docs.gitlab.io/cloud-openstack/index.html
% :topic: index
% :keywords: OPENSTACK, HORIZON, CESGA CLOUD, SSH, DASHBOARD, INSTANCES, NETWORKS, SECURITY GROUPS, VOLUMES, USER GUIDE, OPENSTACK USER GUIDE, COMMAND LINE, VIRTUAL MACHINES, WEB BASED USER INTERFACE.
% :content:

# CLOUD User Guide

Cesga has a CLOUD platform where users may deploy virtual machines. This service is based on [OpenStack.](https://www.openstack.org/)
This guide will explore both the deployment and maintenance of machines through the **Openstack** web based user interface (Horizon)  and the use of virtual machines with command line via ssh.

Horizon is basically a dashboard which enables to create and manage instances, networks, security groups and volumes among others. You can access Horizon at [CESGA CLOUD](https://cloud.srv.cesga.es/auth/login/) but it's recommended to visit [first steps](https://cesga-docs.gitlab.io/cloud-openstack/first_steps.html) before using this service.

:::{Note}
There is more information available at official OpenStack [user guide.](https://docs.openstack.org/mitaka/user-guide/)
:::

**User Guide's Index**

```{toctree}
architecture
glossary
first_steps
keypair
security_groups
volumes
launch_vm
further_assistance
publicity
```
