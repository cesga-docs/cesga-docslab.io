Dissemination and publicity
===========================

For any user which may conduct any research project using the CESGA's services, our center must be properly cited as a collaborating center and mentioned in the acknowledgments section. 

We offer a version in Spanish, Galician and English that should be used. While modifications on the text are permitted, it is essential that the **minimum information** outlined below remains intact:

**English version:**

> This research project was made possible through the access granted by the Galician Supercomputing Center (CESGA) to its cloud platform with funding from the State Programme for the Promotion of Scientific and Technical Research of Excellence of the State Plan for Scientific and Technical Research and Innovation 2013-2016 of the European Regional Development Fund (ERDF), and also with funding from Resolution granting aid for the 2019 call of the Ministry of Science, Innovation and Universities of ERDF, EQC2019-005373-P.

**Galician version:**

> Este proxecto de investigación foi posible grazas ao acceso garantido polo Centro de Supercomputación de Galicia a súa plataforma de cloud, a cal foi financiada polo Programa Estatal de Fomento da Investigación Científica e Técnica de Excelencia, no marco do Plan Estatal de Investigación Científica e Técnica e de Innovación 2013-2016 do FEDER (Fondo Europeo de Desenvolvemento Rexional), e tamén con financiamento procedente da Resolución concesión de axudas convocatoria 2019 do Ministerio de Ciencia, Innovación e Universidades do FEDER, EQC2019-005373-P.


**Spanish version:**

> Este trabajo de investigación se desarrolló gracias al acceso concedido por el Centro de Supercomputación de Galicia (CESGA) a su plataforma de cloud, la cual fue financiada por el Programa Estatal de Fomento de la Investigación Científica y Técnica de Excelencia, en el marco del Plan Estatal de Investigación Científica y Técnica y de Innovación 2013-2016 del FEDER (Fondo Europeo de Desarrollo Regional), y también financiado por la Resolución concesión de ayudas convocatoria 2019 del Ministerio de Ciencia, Innovación y Universidades del FEDER, EQC2019-005373-P.

