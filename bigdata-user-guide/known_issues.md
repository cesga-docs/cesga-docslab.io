% known_issues:

% :title: Known Issues
% :section: bigdata-user-guide
% :url_source: https://cesga-docs.gitlab.io/bigdata-user-guide/known_issues.html
% :topic: known_issues
% :keywords: R-ESSENTIALS, ANACONDA, UNSATISFIABLEERROR
% :content:

# Known Issues

- r-essentials not installing in Anaconda 2019.04: UnsatisfiableError
