% :title: Impala
% :section: bigdata-user-guide
% :url_source: https://cesga-docs.gitlab.io/bigdata-user-guide/impala.html
% :topic: impala
% :keywords: IMPALA, HIVE, SQL, HDFS, INTERACTIVE QUERIES, BATCH QUERIES, SSL, DAEMON, CLUSTER, WORKER NODES
% :content:

(impala)=
# Impala

Like Hive, Impala allows to do interactive SQL queries on HDFS data. It is the recommended option to perform fast small interactive queries because you will benefit from the faster start-up time. To run long batch queries we recommend to use Hive.

Since we are using a secure cluster you have to invoke impala-shell with the --ssl option and you also have to indicate the location of one of the impalad daemons. For example you can launch the impala-shell running:

> impala-shell --ssl --impalad=c14-1

:::{note}
In the impalad option you can indicate any of the worker nodes of the cluster: c14-\[1-14\].
:::

For more information about Impala you can check the [Impala Guide].

[impala guide]: https://www.cloudera.com/documentation/enterprise/6/6.1/topics/impala.html
