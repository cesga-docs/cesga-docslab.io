% CESGA big-data-user-guide User Guide documentation master file

% :title: BD|CESGA Hadoop 3 User Guide
% :section: bigdata-user-guide
% :url_source: https://cesga-docs.gitlab.io/bigdata-user-guide/index.html
% :topic: index
% :keywords: HADOOP, BIG-DATA, USER-GUIDE, CESGA, HADOOP-3, QUICKSTART, KNOWN_ISSUES, YARN, HDFS, SPARK, SPARKLYR, JUPYTER, HIVE, IMPALA, SQOOP, QUOTA, HUE, WEBUI, MIGRATING_DATA
% :content:

# BD|CESGA Hadoop 3 User Guide

```{toctree}
overview
quickstart
whats_new
known_issues
how_to_connect
webui
hue
migrating_data
quota
how_to_upload_data
yarn
hdfs
spark
sparklyr
jupyter
hive
impala
sqoop
modules
want_to_know_more
vpn
publicity
```
