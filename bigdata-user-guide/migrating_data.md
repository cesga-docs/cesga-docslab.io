% :title: Migrating Data
% :section: bigdata-user-guide
% :url_source: https://cesga-docs.gitlab.io/bigdata-user-guide/migrating_data.html
% :topic: migrating_data
% :keywords: HADOOP, HDFS, DISTCP, MIGRATION, DATA, HOME, PLATFORM, HA, NAMESERVICE, SCREEN, SESSION, COMMAND, DIRECTORY, COPY, TARGET, WARNING, NOTE, DECOMMISSIONED
% :content:

(migrating_data)=
# Migrating Data

The data that you have in the HOME have been automatically migrated so no need to move data. Now the old platform is using this same HOME.

To migrate the data in HDFS we recommend that you use the **discp** tool.

For example running the following command it will copy the directory `wcresult` from the old platform to the new one creating the target `wcresult` directory:

```
hadoop distcp -i -pat -update hdfs://10.121.13.19:8020/user/uscfajlc/wcresult hdfs://nameservice1/user/uscfajlc/wcresult
```

:::{warning}
Always run the `distcp` command from the new platform so it takes into account HA (nameservice1 is the HA nameservice ID).
:::

:::{note}
It is recommended to launch the distcp command inside a screen session so it will continue later.
:::

Take into account that the HDFS data that is not migrated from the old platform will be lost once the old platform is decommissioned.
