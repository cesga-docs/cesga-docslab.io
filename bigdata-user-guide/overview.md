% :title: Overview
% :section: bigdata-user-guide
% :url_source: https://cesga-docs.gitlab.io/bigdata-user-guide/overview.html
% :topic: overview
% :keywords: HADOOP, SPARK, HIVE, HBASE, HUE, IMPALA, HOME, HDFS, MIGRATION, DATA
% :content:

(overview)=
# Overview

In this guide we will show you how to use the upgraded BD|CESGA platform based on Hadoop 3.

In the new platform the default version of Spark is also upgraded from 1.6 to 2.4.

There are also new versions of the other main components of the platform like Hive, HBase and HUE.
Additionally Impala is now available.

As previously there are two main filesytems:

- HOME: The standard filesystem when you log in
- HDFS: The distributed Hadoop filesystem

See the {ref}`migrating_data` section for more details about how to migrate your data from the previous platform.

If you want just a quick introduction to get you ready to start using the platform you can jump to the {ref}`quickstart` section.
