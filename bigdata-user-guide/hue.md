% :title: HUE: A nice graphical interface to Hadoop
% :section: bigdata-user-guide
% :url_source: https://cesga-docs.gitlab.io/bigdata-user-guide/hue.html
% :topic: hue
% :keywords: HADOOP, HUE, HIVE, HDFS, YARN, WEBUI, LDAP, FT SUPERCOMPUTER, SPARK UI
% :content:

(hue)=
# HUE: A nice graphical interface to Hadoop

Hadoop User Experience (HUE) allows you to use a web user interface to perform common tasks like submitting new jobs, monitoring existing ones, execute Hive queries or browsing the HDFS filesystem.

The HUE interface can be accessed through the [BD|CESGA WebUI](https://bigdata.cesga.es).

You just have to follow the link to HUE and then login using your credentials (the same as for FT supercomputer).

:::{figure} _static/screenshots/hue-login.png
:align: center

The HUE login page: select LDAP and enter your credentials.
:::

Once inside HUE you can use it to launch Hive queries and display the results in a graphical way.

:::{figure} _static/screenshots/hue-hive.png
:align: center

Launching HiveQL queries from HUE.
:::

Using HUE you also have a quick Web UI to explore HDFS.

:::{figure} _static/screenshots/hue-hdfs.png
:align: center

Exploring HDFS from HUE.
:::

You can also use HUE to monitor your jobs in YARN:

:::{figure} _static/screenshots/hue-jobs-running.png
:align: center

Monitoring jobs using HUE.
:::

And from the properties tab you can get the link to the tracking URL of the job:

:::{figure} _static/screenshots/hue-jobs-properties-url.png
:align: center

Getting the tracking URL of a given job in the properties tab.
:::

Following that link will give you all the information about your job, for example in case of a Spark job you will access to the Spark UI:

:::{figure} _static/screenshots/hue-jobs-to-sparkui.png
:align: center

The Spark UI showing details of a given job.
:::
