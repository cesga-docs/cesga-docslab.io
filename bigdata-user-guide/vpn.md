% :title: ANNEX I: VPN
% :section: bigdata-user-guide
% :url_source: https://cesga-docs.gitlab.io/bigdata-user-guide/vpn.html
% :topic: vpn
% :keywords: VPN, CHECKPOINT, WINDOWS, MACOS, LINUX, CESGA, FINISTERRAE III, PASARELA.CESGA.ES, USERNAME, PASSWORD, CREDENTIALS, HPC, SNX, I386, MULTIARCH, DEPENDENCIES, APT, DPKG, WGET, CHMOD, SUDO, LIBAUDIT1, LIBBSD0, LIBC6, LIBCAP-NG0, LIBGCC-S1, LIBPAM0G, LIBSTDC++5, LIBX11-6, LIBXAU6, LIBXCB1, LIBXDMCP6
% :content:

(vpn)=
# ANNEX I: VPN

The VPN software allows you not only to connect to CESGA in a secure way but also to access internal resources that you can not reach otherwise.

## How to install the VPN software in Windows

Checkpoint VPN is used to establish a secure connection to our services. It enables remote users to securely access our network resources from anywhere in the world using encrypted tunnels, ensuring confidentiality and integrity of data being transmitted over the internet.

To install Checkpoint, you must first download the executable file on [this link.](https://portalusuarios.cesga.es/layout/download/VPN/CheckPointVPN_CESGA_HPC.msi)  Please note that this file is compatible with **Windows 7, 8.1, 10, and 11**. If your Windows version is not one of these, you may encounter some difficulties when installing Checkpoint.

The installation will be carried out with the CheckPointVPN_CESGA_HPC executable file following these steps:

1. Double-click on the executable file. A Windows message will appear indicating that changes will be made to the system. You must accept these changes.
2. Next, the installation wizard will start with a welcome message. Click on Next.

:::{figure} _static/screenshots/vpn1a.png
:align: center
:width: 500px
:::

:::{figure} _static/screenshots/bar.png
:align: center
:width: 100px
:::

3. Next, the License Agreement will appear, it must be accepted by checking the option "I accept the terms in the license agreement" and clicking on Next.

:::{figure} _static/screenshots/vpn2a.png
:align: center
:width: 500px
:::

:::{figure} _static/screenshots/bar.png
:align: center
:width: 100px
:::

4. The next screen will display the **default** directory chosen to save the installation files. It is **recommended** to leave the default path as shown. Then, click on Install.

:::{figure} _static/screenshots/vpn3.png
:align: center
:width: 500px
:::

:::{figure} _static/screenshots/bar.png
:align: center
:width: 100px
:::

5. When the installation starts, a progress bar will appear which should not take more than 5 minutes. Finally, it will show that the installation has finished. Click on Finish.

:::{figure} _static/screenshots/vpn4.png
:align: center
:width: 500px
:::

:::{figure} _static/screenshots/bar.png
:align: center
:width: 100px
:::

6. Automatically after finishing the installation, the **Checkpoint** menu will open:

:::{figure} _static/screenshots/vpn5.png
:align: center
:width: 500px
:::

:::{figure} _static/screenshots/bar.png
:align: center
:width: 100px
:::

As you can see at the top, the site (CESGA-VPN) is already configured by **default**, so it will only be necessary to enter the username and password and click on Connect. If the hostname/IP address is not set by default, complete the server configuration with **pasarela.cesga.es** in the blank space labeled "Server address or name". If you check the "Display name" box, it will allow you to enter an alternative name for the connection such as "CESGA-VPN".

:::{Warning}
**These credentials are the same ones used to access FinisTerrae III or other services offered by CESGA**. That is, it's the username that was granted when registering for CESGA services. **DO NOT ENTER YOUR FULL EMAIL OR DOMAIN @FT3.CESGA.ES**.
:::

For example, if you use <mailto:user_cesga@ft3.cesga.es> to connect to FinisTerrae III or your mail is <mailto:user_cesga@dominion.of.your.center.com> the username that should be entered in the CheckPoint credentials is just **user_cesga**.

Also, if by any reason you are prompted with the window below, please select the option **HPC (default)**.

:::{figure} _static/screenshots/vpn7.png
:align: center
:width: 500px
:::

:::{figure} _static/screenshots/bar.png
:align: center
:width: 100px
:::

When the connection configuration is complete, a window will appear similar to the one shown in step 6. Simply enter your username and password to activate the VPN.

8. Once the credentials are checked it will show that the connection is active.

:::{figure} _static/screenshots/vpn24.png
:align: center
:width: 500px
:::

:::{figure} _static/screenshots/bar.png
:align: center
:width: 100px
:::

:::{Note}
As indicated by the above message, the maximum duration of the VPN connection is **24 hours**. 5 minutes before this time expires, a **notification** will appear to re-enter the password. This will **restart** the connection time counter and allow you to connect for another 24 hours.
:::

**How to log in once Checkpoint is installed?**

Once the CheckPoint client has been installed on your computer and to activate the VPN, you should follow these steps:

1. Look for CheckPoint in your installed applications and open it.
2. The login screen shown in the screenshot of section 6 will appear. As indicated in that section, you should enter your credentials and click on Connect.
3. It is very likely that the program will automatically run when you turn on your computer, so you can find the CheckPoint icon (a yellow padlock) on the desktop taskbar. If you right-click on it, the Connect option will appear and will let you to activate the VPN connection.

If you wish to disconnect from the VPN, on the menu shown in the previous screenshot, you can turn it off by clicking on Shutdown Client.

## How to install the VPN sofware in MacOS

Checkpoint VPN is used to connect to our services. To install the Checkpoint software, you must first download the right version depending on your macOS version:

> - For  macOS 10.14, 10.15, 11 and 12: Download [Checkpoint VPN for macOS](https://portalusuarios.cesga.es/layout/download/VPN/Endpoint_Security_VPN.dmg)
> - For  macOS from 10.11 to 10.13: Use [version 80.89](https://portalusuarios.cesga.es/layout/download/VPN/EndpointSecurityInstalle_80.89.app.zip)
> - For older versions try [version 80.41](https://portalusuarios.cesga.es/layout/download/VPN/EPS_VPN_E80.41_Mac.dmg)  However, we cannot guarantee that it will work on every older version.

The installation will be carried out with any of the excutable files described above and following the steps of the wizard. Be careful, the server/hostname/IP address **is not set by default** on macOS, so you will have to complete the configuration being the hostname/IP address **pasarela.cesga.es**.

:::{figure} _static/screenshots/vpn_mac.png
:align: center
:width: 500px
:::

:::{figure} _static/screenshots/bar.png
:align: center
:width: 100px
:::

As shown on the screenshot above, you have to add **pasarela.cesga.es** on the blank space of "Server address or name". If you check the "Display name" box, It would let you to write and alternative name for the connection, for example "CESGA-VPN".

:::{figure} _static/screenshots/vpn7.png
:align: center
:width: 500px
:::

:::{figure} _static/screenshots/bar.png
:align: center
:width: 100px
:::

If, by any reason, you are prompted with the window above please select the option **HPC (default)**.

Once the configuration of the server is made and you connect the VPN, it will prompt you to add your user and password. The credentials to log in have the same warning as in the others OS:

:::{Warning}
**These credentials are the same ones used to access FinisTerrae III or other services offered by CESGA**. That is, it's the username that was granted when registering for CESGA services. **DO NOT ENTER YOUR FULL EMAIL OR DOMAIN @FT3.CESGA.ES**.
:::

For example, if you use <mailto:user_cesga@ft3.cesga.es> to connect to FinisTerrae III or your mail is <mailto:user_cesga@dominion.of.your.center.com> the username that should be entered in the CheckPoint credentials is just **user_cesga**.

## How to install the VPN software in Linux

Checkpoint VPN is used to connect to our services. In Linux we will use the **snx** client to connect. Just follow the steps explained below:

1. From the command line of your computer, download the snx file executing:

   ```
   wget http://bigdata.cesga.es/files/snx
   ```

2. Change the permissions of the file to make it executable:

   ```
   chmod a+x snx
   ```

3. Install the **required dependencies**, multiarch must be enable because snx is a i386 binary:

   ```
   sudo dpkg --add-architecture i386
   sudo apt update
   sudo apt install libaudit1:i386 libbsd0:i386 libc6:i386 libcap-ng0:i386 libgcc-s1:i386 libpam0g:i386 libstdc++5:i386 libx11-6:i386 libxau6:i386 libxcb1:i386 libxdmcp6:i386
   ```

4. Once the installation is complete, to start the VPN connection you must execute the following command: `sudo ./snx -s pasarela.cesga.es -u <username>` You will need to enter your username and password.

:::{warning}
The \<username> is your cesga username eg. uscfajlc. Do not confuse with your email address.
:::

5. It will prompt you to enter your password, and once the connection is established, it will display the message:

   ```
   Check Point's Linux SNX
   build 800010003
   Please enter your password:
   NX - connected.
   Session parameters:
   ===================
   Office Mode IP      : ...
   DNS Server          : ...
   Secondary DNS Server: ...
   Timeout             : 24 hours
   ```

As indicated by the above message, the maximum duration of the VPN connection is **24 hours**. 5 minutes before this time expires, a **notification** will appear to re-enter the password. This will **restart** the connection hours counter and allow you to connect for another 24 hours.

6. To disconnect the VPN, use the following command:

   ```
   sudo snx -d
   ```
