% :title: Want to know more
% :section: bigdata-user-guide
% :url_source: https://cesga-docs.gitlab.io/bigdata-user-guide/want_to_know_more.html
% :topic: want_to_know_more
% :keywords: HDFS, YARN, SPARK, JUPYTER, HIVE, SQOOP, MAPREDUCE, PYSPARK, SPARKLYR, CDH, CLOUDERA, TUTORIALS, DOCUMENTATION, GUIDES, REFERENCE, COMPONENT, HUE, IMPALA, BIGDATA, CESGA, CLUSTERS.
% :content:

(want_to_know_more)=
# Want to know more

For more information we recommend that you start looking at the tutorials that we have prepared to get you started with each of the tools.

After that, if you need additional information you can check CDH documentations with the different component guides as well as the official documentation of each component.

## Tutorials

You can find the complete list of tutorials at <https://bigdata.cesga.es/#tutorials>

- [HDFS Tutorial]
- [YARN Tutorial]
- [Spark Tutorial]
- [Jupyter Tutorial]
- [Hive Tutorial]
- [Sqoop Tutorial]
- [MapReduce Tutorial]

Additionally you can find useful the material of the PySpark and Sparklyr courses:

- [PySpark Course Material]
- [Sparklyr Course Material]

## CDH Documentation

- [CDH Documentation] for CDH 6.1
- [Spark Guide]
- [Hive Guide]
- [Impala Guide]
- [HUE Guide]

## Reference Documentation

In the following page you can find the links to the [Reference documentation for each component].

[cdh documentation]: https://www.cloudera.com/documentation/enterprise/6/6.1/topics/cdh_components.html
[hdfs tutorial]: https://bigdata.cesga.es/tutorials/hdfs.html
[hive guide]: https://www.cloudera.com/documentation/enterprise/6/6.1/topics/hive.html
[hive tutorial]: https://bigdata.cesga.es/tutorials/hive.html
[hue guide]: https://www.cloudera.com/documentation/enterprise/6/6.1/topics/hue.html
[impala guide]: https://www.cloudera.com/documentation/enterprise/6/6.1/topics/impala.html
[jupyter tutorial]: https://bigdata.cesga.es/tutorials/hdfs.html
[mapreduce tutorial]: https://bigdata.cesga.es/tutorials/hdfs.html
[pyspark course material]: https://github.com/javicacheiro/pyspark_course
[reference documentation for each component]: https://www.cloudera.com/documentation/enterprise/6/6.1/topics/cdh_component_documentation_links.html
[spark guide]: https://www.cloudera.com/documentation/enterprise/6/6.1/topics/spark.html
[spark tutorial]: https://bigdata.cesga.es/tutorials/spark.html
[sparklyr course material]: https://github.com/aurora-mareviv/sparklyr_test
[sqoop tutorial]: https://bigdata.cesga.es/tutorials/hdfs.html
[yarn tutorial]: https://bigdata.cesga.es/tutorials/yarn.html
