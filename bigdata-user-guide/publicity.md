Dissemination and publicity
===========================

For any user which may conduct any research project using the CESGA's services, our center must be properly cited as a collaborating center and mentioned in the acknowledgments section. 

We offer a version in Spanish, Galician and English that should be used. While modifications on the text are permitted, it is essential that the **minimum information** outlined below remains intact:

**English version:**

> This research project was made possible through the access granted by the Galician Supercomputing Center (CESGA) to its Big Data platform with funding from the State Programme for the Promotion of Scientific and Technical Research of Excellence of the State Plan for Scientific and Technical Research and Innovation 2013-2016 of the European Regional Development Fund (ERDF), CESG13-1E-1790.

**Galician version:**

> Este proxecto de investigación foi posible grazas ao acceso garantido polo Centro de Supercomputación de Galicia a súa plataforma de Big Data, a cal foi financiada polo Programa Estatal de Fomento da Investigación Científica e Técnica de Excelencia, no marco do Plan Estatal de Investigación Científica e Técnica e de Innovación 2013-2016 do FEDER (Fondo Europeo de Desenvolvemento Rexional), CESG13-1E-1790.


**Spanish version:**

> Este trabajo de investigación se desarrolló gracias al acceso concedido por el Centro de Supercomputación de Galicia (CESGA) a su plataforma de Big Data, la cual fue financiada por el Programa Estatal de Fomento de la Investigación Científica y Técnica de Excelencia, en el marco del Plan Estatal de Investigación Científica y Técnica y de Innovación 2013-2016 del FEDER (Fondo Europeo de Desarrollo Regional), CESG13-1E-1790.

