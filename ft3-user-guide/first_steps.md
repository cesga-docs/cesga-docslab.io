(ft3_first_steps)=

% :title: First steps
% :section: ft3-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft3-user-guide/first_steps.html
% :topic: first_steps
% :keywords: USER, REGISTRATION, GROUP, CESGA, VPN, SSH, CLIENT, ENCRYPTION, AUTHENTICATION, SECURITY, PASSWORD, SCP, SFTP, X-WINDOWS, WINDOWS, VS CODE, OPENSSL, MACS, HMAC-SHA2-512, COMMAND-LINE, POWERSHELL, SCP, AUTHORIZATION, REQUEST
% :content:

# First steps

## Creating an account

The initial step entails [registering as a user.](https://altausuarios.cesga.es/user/ualta) We highly recommend following the outlined steps on the website and reviewing the user registration diagram to avoid any potential issues with your application.

For members of CSIC who intend to create a group, a group registration must be requested through [group registration](https://altausuarios.cesga.es/user/galta) and follow the corresponding steps outlined on the website.

## Connecting with CESGA

To ensure security, access to our servers is strictly limited to authorized centers, which include Galician universities, CSIC centers, and centers with special agreements. Therefore, only registered users from these centers are allowed to access the servers. Access from outside of these authorized centers must use VPN. For additional information regarding the VPN and its configuration, please refer to the following steps at [how to connect.](https://cesga-docs.gitlab.io/ft3-user-guide/how_to_connect.html#configure-vpn)

The secure access to our servers requires an SSH (Secure Shell) client that enables encrypted information transfer. SSH allows remote connection between computers over a network, facilitating the execution of commands on remote machines and the transfer of files between them. It provides strong authentication and ensures secure communication over non-secure channels.  All communications are automatically and transparently encrypted including passwords, which eliminates the possibility of password capture, a common practice by which computer system security is compromised. Most versions of SSH also offer remote copy (SCP) functionality, and many provide a secure FTP (SFTP) client.  Additionally, SSH allows secure X-Windows connections.

:::{Warning}
**Workaround for Windows SSH and VS Code Users**

Some people who use Windows computers to ssh to our machines from a Windows command prompt, powershell, or via Visual Studio Code's SSH extension have received a new error message about a "*Corrupted MAC on input*" or "*message authentication code incorrect*". This error is due to an outdated OpenSSL library included in Windows and a recent security-mandated change to ssh on our machines. However, there is a functional workaround for this issue. (**Note: If you are not experiencing the above error, you do not need and should not use the following workaround.**)

For VS Code SSH extension users, you will need to create an ssh config file on your local computer or modify it (`~/.ssh/config`), with a host entry for our machines that specifies a new message authentication code. For example, for FinisTerrae III the content to add to this file could be somethink like this:

**Host ft3.cesga.es**

> **HostName ft3.cesga.es**
>
> **MACs hmac-sha2-512**

Here `~` means `C:\Users\user_that_you_have_in_your_computer`. **The configuration file will also apply to command-line ssh in Windows, as well**.

For command-line and Powershell ssh users, adding `-m hmac-sha2-512` to your ssh command is another solution. For example: `ssh -m hmac-sha2-512 <username>@ft3.cesga.es`. If you are using the scp command, you have to add `-o MACs=hmac-sha2-512`.
:::

:::{note}
This [authorization request](https://altausuarios.cesga.es/solic/conex) is intended for new center asociations or for users which need mandatory the registration of their public IP to granteed access due to technical problems.
:::
