(ft3_batch_email_warning)=

% :title: Email notifications
% :section: ft3-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft3-user-guide/batch_email_warning.html
% :topic: batch_email_warning
% :keywords: SBATCH, EMAIL, NOTIFICATION, TYPE, USER, EVENT, DEPENDENCY, TIME LIMIT, ARRAY TASKS, MAIL-USER, MAIL-TYPE, JOB, BATCH, SLURM.
% :content:

# Email notifications

When sending a job we may be interested in knowing when it starts, when
it ends, if it has failed, etc. This can be acomplished by setting
notifications to our email:

```
--mail-type=<type>
```

Notify the user  by  email  when  certain event types occur. Multiple
type values may be specified in a comma  separated  list.

\<type> could be:

- BEGIN, END, FAIL
- INVALID_DEPEND: dependency never satisfied
- TIME_LIMIT_90: reached 90 percent of time limit
- TIME_LIMIT_80: reached 80 percent of time limit
- TIME_LIMIT_50: reached 50 percent of time limit
- ARRAY_TASKS: send emails for each array task

```
--mail-user=<user_email_address>
```

User email address to receive email notification of state changes
as defined by --mail-type.

Example:

```
#!/bin/sh
#SBATCH -N 2 #(solicita dos nodos)
#SBATCH -n 2 --mem-per-cpu=1G #(dos tareas en total)
#SBATCH -t 00:00:30 #(30 sec ejecución)
#SBATCH --mail-type=begin #Envía un correo cuando el trabajo inicia
#SBATCH --mail-type=end #Envía un correo cuando el trabajo finaliza
#SBATCH --mail-user=tec_sis1@domain.com #Dirección a la que se envía
srun hostname
```

`sbatch ./job.sh`

Submitted batch job 16899

The notifications sended to the specified email address will be:

:::{figure} _static/screenshots/batch_email_warning.png
:align: center
:::
