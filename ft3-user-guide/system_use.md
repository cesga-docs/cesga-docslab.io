(ft3_system_use)=

% :title: System use
% :section: ft3-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft3-user-guide/system_use.html
% :topic: system_use
% :keywords: SYSTEM USE, INTERACTIVE USE, REMOTE DESKTOPS, DEDICATED INTERACTIVE NODES, QUEUEING SYSTEM, RESOURCES, INTERACTIVITY, SIMULATIONS, VIRTUAL MEMORY, GRAPHICAL INTERFACE, APPLICATIONS, MEMORY, CORES, TIME, ALLOCATION, USERS
% :content:

# System use

There are four methods for using the equipment, depending on the required resources and interactivity:

- **Interactive use:** when connecting to the computer you have access to limited and shared resources, insufficient for performing simulations, but fine enough for working with files and interact with the system.  The maximum virtual memory available is 8GB.
- [Remote Desktops:](https://cesga-docs.gitlab.io/ft3-user-guide/remote_desktops.html)  for remote desktop access users can visit [this URL](https://portalusuarios.cesga.es/tools/remote_vis) through which they have access to a complete graphical X11 system to display and access applications with a graphical interface. However, long simulations or the use of more than 32GB of memory is not possible.
- **Dedicated interactive nodes:** Users can initiate interactive sessions with dedicated resources by using the command `compute` (for more information it can be used with the option `--help`) from the node to which they are connected . Nonetheless, the maximum resources available are 64 cores and 247GB of RAM memory for a maximum of **8 hours**.
- **Using the queueing system:** To perform simulations requiring multiple cores and significant amounts of memory and time, it is necessary to use the queueing system, which guarantees the allocation of resources to each user.
