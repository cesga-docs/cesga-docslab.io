(ft3_scratch_dirs)=

% :title: Temporary storage (Scratch)
% :section: ft3-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft3-user-guide/scratch_dirs.html
% :topic: scratch_dirs
% :keywords: LUSTRE, SCRATCH, TEMPORARY STORAGE, $LUSTRE_SCRATCH, $LOCAL_SCRATCH, $TMPDIR, $TMPSHM, TMPFS, IO, MEMORY LIMIT, JOB, NODE.
% :content:

# Temporary storage (Scratch)

In addition to directly using the \$LUSTRE directory (Lustre high speed NVMe storage), several temporary
directories are created when jobs are sent to the queue system, which
are automatically deleted when the job is finished. These directories
are, in the case of FinisTerrae III nodes:

> - **\$LUSTRE_SCRATCH**: temporary directory on Lustre shared storage.
> - **\$LOCAL_SCRATCH** or \$TMPDIR: temporary directory on the local disk of each node.
> - **\$TMPSHM**: temporary storage directory in the node's own RAM memory ([tmpfs](https://en.wikipedia.org/wiki/Tmpfs)). You must take into account the space occupied by the scratch directories in memory. This directory can be used in case you need to perform very intensive IO on a file or set of files.

:::{warning}
You must take into account that the space occupied by the
scratch directories in memory counts towards the memory limit of the job
and cannot exceed half of the physical memory of the node.
:::
