(ft3_data_transfer)=

% :title: Data transfer
% :section: ft3-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft3-user-guide/data_transfer.html
% :topic: data_transfer
% :keywords: SCP, SFTP, RSYNC, SCREEN, MOBAXTERM, WINSCP, FILEZILLA, ASPERA, DTN, CESGA, VPN, SSH, SFTP CLIENTS, DATA TRANSFER, LARGE-SCALE DATA TRANSFERS, TERMINAL MULTIPLEXER, FILE TRANSFER ENVIRONMENT, USER-FRIENDLY INTERFACE, DRAG AND DROP, LOCAL COMPUTER, FINISTERRAE III, DIRECTORY NAVIGATION, USERNAME, PASSWORD, HOSTNAME, PORT, PUBLIC IP ADDRESS, SYSTEMS DEPARTMENT, COLLABORATION, EXTERNAL USERS, CESGA SERVICES.
% :content:

# Data transfer

To transfer files to or from the FinisTerrae III, you will need to use an *scp* or *sftp* client or the `rsync` command.  For large data transfers (GB or even TB), there are two special nodes available called dtn-1 and dtn-2. There is more information about this nodes down below.

In case of interruption, running the `rsync` command will resume the transfer from the point where it was cut off. The recommendation is to run it several times to ensure that no files remain unsynchronized.

It could be recommended to use the command `screen` to execute these commands to transfer these data, because it allows you to resume the sessions. Screen is a terminal multiplexer. In other words, it means that you can start a screen session and then open any number of windows (virtual terminals) inside that session. Processes running in Screen will continue to run when their window is not visible even if you get disconnected. If you use this command, you have to remember the login node where you have launched it, to be able to resume your screen session.

## MobaXterm

As explained on [how to connect,](https://cesga-docs.gitlab.io/ft3-user-guide/how_to_connect.html) **MobaXterm** is a tool that provides both SSH access and a file transfer environment. Its user-friendly interface makes it easy to transfer files without using complex commands, especially for new users. To transfer files, simply navigate to the FinisTerrae III directory where you want to move the files using the left panel of the program interface. Then, drag the files from your local computer folder to the left panel of the console.

You can also use the same methodology to transfer files from FinisTerrae III to your local computer by dragging the file icon.

## SCP and SFTP clients

**WinSCP** is a tool that provides an environment for data transferring. To configure the connection to FinisTerrae III, follow these steps:

```console
Protocol: SFTP
Name or server IP: ft3.cesga.es
Port: 22
Username: username (just the name, not the full email)
Password: your_password
```

Once connected, you will have your local computer on the left and FinisTerrae III directories on the right. Data transfer can be achieved by simply dragging files from one directory to the other. Additionally, you can navigate between directories on both sides to locate the files you want to transfer.

Note that there are several other SCP and SFTP clients that can be used, and the ones mentioned above are just examples.

## DTN nodes

As mentioned above, the DTN nodes are intended for large-scale data transfers and are **exclusively** designed for this purpose. Any interaction that is **not related** to data transfer **will be removed**, and the user **may face penalties**. There are two ways to access these nodes, depending on whether or not you are a CESGA user.

- **Access to the DTN nodes for CESGA users:**

Accessing these nodes is similar to accessing the FinisTerrae-III login nodes. The DTN nodes can be accessed using the hostname `dtn.srv.cesga.es`
The username and password are the same as those used for FinisTerrae III. You can use the `rsync` or transfer commands  to start the data transfer, and they can also be directly accessed via sftp using the following command: `sftp username@dtn.srv.cesga.es`

It is also possible to use WinSCP, FileZilla, or similar clients to perform the data transfer using the following configuration:

```console
Hostname: dtn.srv.cesga.es
Port: 22
Username: username
Password: your_password
```

Also, ASPERA is available on these nodes for users who prefer or are able to use it as an alternative method for data transfer.

:::{Warning}
VPN access to these nodes is NOT POSSIBLE. Therefore, data transfers must be initiated from an authorized center. In exceptional cases where a user is outside of these centers, we can add their public IP address to grant them access. If this applies to you, please contact the Systems Department and provide information about the data transfer you wish to perform, the public IP from which the connection will be made and the approximate duration of the connection so that we can grant access to that IP.
:::

- **Access to the DTN nodes for external users:**

External users refer to those who are not registered in our services. Data transfer between CESGA users and external users is possible, for both uploading and downloading data from both parties. A possible scenario for this exchange would be sharing data with external collaborators of our users. If this is the case, the protocol to grant access is:

By the CESGA user, contact the Systems Department, explaining that you need to transfer data (or receive data) from an external collaborator.  If the transfer is from the CESGA user to the external collaborator, please indicate the directory where the data is located. The external collaborator will be provided with the necessary credentials to access the DTN nodes through a username and password. In this case, the connection is only allowed by the `sftp` command.

You must also provide the public IP of the external user to grant access (as previously mentioned, these nodes cannot be accessed via VPN, and the IPs must be included in our system). Additionally, you must indicate the approximate time frame during which the service will be offered to perform the data transfer.
