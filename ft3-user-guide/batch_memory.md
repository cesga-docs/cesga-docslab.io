(ft3_memory)=

% :title: Memory and time
% :section: ft3-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft3-user-guide/batch_memory.html
% :topic: batch_memory
% :keywords: MEMORY, TIME, JOB SUBMISSION, SBATCH, MEM, MEM-PER-CPU, SBATCH_MEM_PER_NODE, SBATCH_MEM_PER_CPU, SEFF, EXECUTION, CPU, WALL-CLOCK, MEMORY UTILIZED, MEMORY EFFICIENCY, CORES, CPU UTILIZATION, PU EFFICIENCY, JOB WALL-CLOCK TIME, TOTAL MEMORY, TASK, CORE, SCRIPT
% :content:

# Memory and time

:::{warning}
Both parameters are mandatory for job submission.
:::

Memory demand for the job must be specified with the options: `--mem` or `--mem-per-cpu`.
: - Input environment variable: **SBATCH_MEM_PER_NODE**
  - Input environment variable: **SBATCH_MEM_PER_CPU**

Maximum execution time must be set using: `--time=D-HH:MM:SS` (or in reduced form `-t D-HH:MM:SS`)
: - Input environment variable: **SBATCH_TIMELIMIT**

Sometimes it can be very difficult to determine how much memory a job will require. For this purpose, we recommend that you conduct tests with different amounts of memory. You can use the `seff <jobid>` command to check the actual memory used by your job and adjust your memory request accordingly. The output of this command is approximate, so it should not be taken literally but rather as an approximation:

```
$ seff 2106200
Job ID: 2106200
Cluster: finisterrae3
User/Group:
State: COMPLETED (exit code 0)
Cores: 1
CPU Utilized: 1-22:39:55
C  PU Efficiency: 99.64% of 1-22:50:01 core-walltime
Job Wall-clock time: 1-22:50:01
Memory Utilized: 129.04 MB
Memory Efficiency: 4.20% of 3.00 GB
```

For this example, the job request of 3GB of RAM memory seems too high, so it should be adjusted.

These are some examples of job submission commands:

- 1 task job using 1 core requesting 6GB of total memory and 1 hour execution:

  ```
  $ sbatch -n 1 --mem=6GB script.sh --time=01:00:00
  ```

- 1 task job using 2 core requesting 40GB of total memory (20 GB per core) and 1 hour execution:

  ```
  $ sbatch -n 1 --cpus-per-task=2 --mem-per-cpu=20GB script.sh -t 01:00:00
  ```
