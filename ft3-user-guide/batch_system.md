(ft3_batch_system)=

% :title: Batch system: Submitting jobs with SLURM
% :section: ft3-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft3-user-guide/batch_system.html
% :topic: batch_system
% :keywords: SLURM, QUEUEING SYSTEM, LINUX CLUSTERS, JOB SCHEDULING, COMPUTE NODES, RESOURCES, JOBS, MONITORING, CONTENTION, ARBITRATION, GUIDES, TUTORIALS.
% :content:

# Batch system: Submitting jobs with SLURM

The queueing system is based on SLURM, an open source, fault-tolerant and highly scalable cluster management and job scheduling system for Linux clusters.

Slurm has three key functions:
: 1. It allocates exclusive and/or non-exclusive access to resources (compute nodes) to users for some duration of time.
  2. It provides a framework for starting, executing and monitoring jobs on the set of allocated nodes.
  3. It arbitrates contention for resources by managing a queue of pending work.

For more information about SLURM you can consult the guides and tutorials available on their website: [Quickstart user guide](http://www.schedmd.com/slurmdocs/quickstart.html) and  [SLURM tutorials.](http://www.schedmd.com/slurmdocs/tutorials.html) There's also some useful links at [portalusuarios.cesga.es](https://portalusuarios.cesga.es/info/links)

**Index of contents:**

```{toctree}
batch_memory
batch_partitions
batch_qos
batch_basic_commands
batch_examples
batch_job_array
batch_multiple_tasks
batch_binding_tasks_specifics_cores
batch_jobs_states
batch_stdout_err
batch_email_warning
```
