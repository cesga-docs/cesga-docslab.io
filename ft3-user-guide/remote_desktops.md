(ft3_remote_desktops)=

% :title: Remote desktops and visualization tools
% :section: ft3-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft3-user-guide/remote_desktops.html
% :topic: remote_desktops
% :keywords: REMOTE DESKTOPS, VISUALIZATION TOOLS, USER PORTAL, VPN, QUOTA LIMITS, STORAGE SPACE, TERMINAL ICON, NEW TERMINAL WINDOW, FIREFOX ICON, CLIPBOARD, $HOME QUOTA, PERMANENT STORAGE, DESKTOPS CLI, BASH AUTOCOMPLETION, JUPYTER NOTEBOOK, COMPUTE INTERACTIVE SESSION, GPU, MEMORY, VISUALIZATION RESOURCES, START_JUPYTER, START_JUPYTER-LAB, CESGA'S VPN.
% :content:

# Remote desktops and visualization tools

## Remote desktops

Remote desktops are a tool designed for accessing FinisTerrae III in a visualization mode that is more user-friendly for certain users. To create a remote desktop, navigate to the User Portal > Tools > [Remote visualization FT-III.](https://portalusuarios.cesga.es/tools/remote_vis) There is **no need to use the VPN** to connect to the remote desktops, making this tool very useful in the event of potential VPN connection issues.

Once you have created a desktop, it will remain active for a maximum of 36 hours. If you do not extend the duration of your session, the desktop will automatically close, and any unsaved data will be lost.

- To monitor the **remaining time** for your desktop, check the top bar. By clicking on it, you can extend your session for an additional 36 hours. Please note that changes may take up to 2 minutes to become effective and will not be immediate.
- If you slide your mouse over the circular graphic icon, you can view your **Home** [quota limits](https://cesga-docs.gitlab.io/ft3-user-guide/storage.html), including those for file and storage space.
- The **screen icon** is intended to adjust the resolution of the remote desktop to match that of the user's local screen.
- **Terminal icon** will open a **new terminal window** at user's Home directory of the FinisTerrae III.
- **Firefox icon** will launch a new window and open the website.
- **Clipboard**: is the left panel used to facilitate communication between your local PC and the remote desktop. It includes a keyboard, a clipboard that allows copying and pasting between your local PC and the remote desktop, a full-screen mode, settings and a disconnection icon.

:::{warning}
If you experience issues while opening a Remote Desktop from both the User Portal and SSH sites, it may be due to exceeding your **\$HOME quota** or approaching the limit. In such cases, you will not be able to create a Remote Desktop. To solve this issue, free up space in your \$HOME directory by deleting any unnecessary data or moving it to your \$STORE or \$LUSTRE directories. For further information about storage and quota limits, please refer to: [Permanent Storage.](https://cesga-docs.gitlab.io/ft3-user-guide/storage.html)
:::

## Desktops CLI

FinisTerrae III also provides a desktops CLI that you can use to manage your remote desktops. The CLI is preconfigured with bash autocompletion and supports the following commands:

- `desktops`: allows you to manage your remote desktops from the command line.
- `desktops create`: creates a new desktop and returns the URL.
- `desktops delete`: erases the desktop. This command can be used as a backup option to delete the desktop if the option available on the User Portal is not working for any reason.
- `desktops extend`: extends the time for the remote desktop by 36 hours.
- `desktops --help`: provides additional information on how to use the command.

## Jupyter notebook

Jupyter Notebook environment is presented in the form of a web document that combines text, code, and visualizations. This makes it an ideal tool for data exploration and visualization, as well as for creating interactive reports and presentations. It is installed as a base module in our system, and there are several ways to use it.

:::{warning}
When you connect to FinisTerrae III, you are automatically redirected to a login node, but these nodes are not for computing tasks or for launching jupyter notebooks. Based on the type of resources you need (GPU: a100 or T4, memory, visualization...) you have to request a `compute` interactive session and then launch the notebook there.
:::

So to launch a Jupyter notebook first we will create a `compute` interactive session with the resources that we need, for example if we need 1 core, 4GB of RAM and a GPU we can use:

```
compute --cores 1 --mem 4 --gpu
```

and then we launch the notebook with:

```
start_jupyter
```

We can also use the new JupyterLab notebooks using:

```
start_jupyter-lab
```

Then we simply copy the URL provided in our browser and, if we have the CESGA's VPN active, then we will be able to access the notebook.

:::{Warning}
To connect to the notebook you must have the VPN must be active, even if you are connected from an authorized center. This is because the connection is made to a private internal address that is only accessible from the VPN.
:::

Of course, you can also start a notebook inside a remote desktop. In this case the VPN is not required and your notebook will be running on a node with a NVIDIA T4 GPU.
