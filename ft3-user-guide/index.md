% CESGA FT 3 User's Guide documentation master file

% :title: FinisTerrae III User Guide
% :section: ft3-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft3-user-guide/index.html
% :topic: index
% :keywords: FT3, FEDERFT3, USER GUIDE, INDEX, OVERVIEW, FIRST STEPS, GLOSSARY, HOW TO CONNECT, SYSTEM USE, REMOTE DESKTOPS, DATA TRANSFER, STORAGE, SCRATCH DIRS, PARALLELIZATION, BATCH SYSTEM, BATCH BINDING, CRON, JOB SIGNALS, JOB REQUEUE, COMPILERS AND DEV TOOLS, ENV MODULES, FAT NODES, GPU NODES, OTHER NODES, FAQ, PUBLICITY
% :content:

::::{list-table}
:header-rows: 0

* - :::{figure} _static/screenshots/Cartel-FEDER-FTIII-800x571.png
    :width: 320px
    :align: center
    :::
  - :::{figure} _static/screenshots/ft3.png
    :width: 320px
    :align: center
    :::
::::

# FinisTerrae III User Guide

```{toctree}
overview
first_steps
glossary
how_to_connect
system_use
remote_desktops
data_transfer
storage
scratch_dirs
parallelization
batch_system
batch_binding
cron
job_signals
job_requeue
compilers_and_dev_tools
env_modules
fat_nodes
gpu_nodes
other_nodes
FAQ
publicity
want_to_know_more
