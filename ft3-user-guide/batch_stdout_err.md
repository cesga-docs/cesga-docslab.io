(ft3_batch_stdout_err)=

% :title: Output files
% :section: ft3-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft3-user-guide/batch_stdout_err.html
% :topic: batch_stdout_err
% :keywords: SLURM, STDOUT, STDERR, SBATCH, JOB_NUMBER, ARRAY_INDEX, OUTPUT, ERROR, FILE, DIRECTORY
% :content:

# Output files

By default, slurm combines standard and error output into a single file
called **slurm-\<job_number>.out** or
**slurm-\<job_number>\_\<array_index>.out** if it's a job array. By default
these files are saved in the submission directory. This behavior can be
modified with the following options of the `sbatch` command:

`--error /path/to/dir/filename`

`--output /path/to/dir/filename`
