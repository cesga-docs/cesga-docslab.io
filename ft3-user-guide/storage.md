(ft3_storage)=

% :title: Permanent storage
% :section: ft3-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft3-user-guide/storage.html
% :topic: storage
% :keywords: STORAGE, PARTITION, MEMORY, SPACE, FILE, LIMITS, QUOTA, HOME, REQUEST, DIRECTORY, USE, USER, LIMITS, BACKUP, SNAPSHOT, COPY, FILES, RECOVERY, ACCESS, FILESYSTEMS, QUOTAS, USAGE, LIMIT, NUMBER, FILES, TOTAL, SPACE, COMMAND, MYQUOTA, FILESYSTEM, SIZE, USED, AVAIL, MOUNTED, LUSTRE, SCRATCH
% :content:

# Permanent storage

The storage system is divided into three principal partitions, each one with different memory space and file limits. To avoid unexpected failures we recommend verifying that you have enough space for your jobs before submitting them. Special care must be observed in the HOME quota. Inability to write to the home will cause many services and applications to crash.

If you are close to reach your quota and you need to increase the limits, you can ask for an [Additional Storage Request].

```{eval-rst}
+--------------------+---------------------------------+-----------------+------------+
| **Directory**      | **Use**                         | **User limits** | **Backup** |
+--------------------+---------------------------------+-----------------+------------+
|                    | Store code files                | 10GB            | Yes        |
|      $HOME         |                                 |                 |            |
|                    | Low speed access                | 100.000 files   |            |
+--------------------+---------------------------------+-----------------+------------+
|                    | Store simulations final results | 500GB           | No         |
|      $STORE        |                                 |                 |            |
|                    | Low speed access                | 300.000 files   |            |
+--------------------+---------------------------------+-----------------+------------+
|                    | Simulation runs                 | 3TB             | No         |
|      $LUSTRE       |                                 |                 |            |
|                    | High speed access               | 200.000 files   |            |
+--------------------+---------------------------------+-----------------+------------+
```

The copy of the \$HOME directory is made and stored for only **one week**, so it is more of a **snapshot** than a backup. Only files from more than a week ago can be recovered upon request. To access the snapshot copies, enter FinisTerrae III normaly and execute `cd .snapshot` from you \$HOME directory. To list the files, use `ls`.

You will see several folders which are copies of your \$HOME directory from each of the past week's days. This copy is automatically made during the night, so depending on when you deleted or added files, you may need to choose the appropriate folder from which to make the restore. To access the folders, simply execute `cd daily.selected-date`.

To copy only certain files to your \$HOME directory, execute:
`cp .snapshot/daily.selected-date/rest-of-path-to-file /home/wherever-you-want-to-recover-the-file`

It is recommended to only copy the necessary files and **not to copy the entire daily directory**. You could fill up your \$HOME space and cause a failure. It is also recommended to use **absolute paths** on the above command.

## Filesystem Quotas

The filesystems have usage quotas that limit both the maximum allowed number of files and the total space used.
To see your current filesystem quotas and how close you are to reach the limits you can use the `myquota` command.

```console
[swtest@login210-19 ~]$ myquota

  - HOME and Store filesystems:
    --------------------------
Filesystem               space  quota   limit   files  quota  limit
10.117.49.201:/Home_FT2  3200M  10005M  10240M  42506  100k   101k
10.117.49.101:/Home_BD   8K     800G    1024G   7      4295m  4295m

 - Special filesystems:
   -------------------
Filesystem                  Size  Used Avail Use% Mounted on
10.117.49.101:/Store_CESGA   13T  9.2T  3.9T  71% /mnt/netapp1/Store_CESGA
total                        13T  9.2T  3.9T  71% -

 - LUSTRE filesystem:
   -----------------
Filesystem    used   quota   limit   grace   files   quota   limit   grace
/mnt/lustre/scratch
                   4k      3T    3.5T       -       1  200000  240000       -
```

[additional storage request]: https://altausuarios.cesga.es/solic/almac
