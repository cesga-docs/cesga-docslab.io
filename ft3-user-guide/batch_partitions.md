(ft3_partitions)=

% :title: Partitions
% :section: ft3-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft3-user-guide/batch_partitions.html
% :topic: batch_partitions
% :keywords: NODES, PARTITIONS, CORES, RAM, GPU, A100, OPTANE, T4, DTN, DATA TRANSFER, BATCHLIM, QOS, LIMITS, CPUS, MEMORY, ONDEMAND, LONG, MEDIUM, REQUEUE, SHORT, VIZ, CLK, AMD EPYC,ILK, SMP
% :content:

# Partitions

Among the **431 nodes** of the cluster, **354** were accquired specifically for FinisTerrae III and the remaining were recovered from previous clusters of our infraestructure. All of them are shared between the following partitions:

- **short, medium, long, ondemand and requeue partitions:** sharing the bulk of nodes (431 nodes) encompassing the following groups:
  : - [ilk:](https://cesga-docs.gitlab.io/ft3-user-guide/overview.html) 256 nodes with 64 cores and 246GB of RAM available.
    - [smp:](https://cesga-docs.gitlab.io/ft3-user-guide/fat_nodes.html) 16 nodes with 64 cores and 2011GB of RAM available + 1  node with 64 cores and 7975GB (Optane).
    - [a100:](https://cesga-docs.gitlab.io/ft3-user-guide/gpu_nodes.html#nvidia-a100) 64 nodes with 64 cores, 246GB of RAM available and 2 GPU A100 + 1 node with 5x NVIDIA A100 + 1 node with 8x NVIDIA A100.
    - [clk:](https://cesga-docs.gitlab.io/ft3-user-guide/other_nodes.html#intel-cascade-lake-nodes) 74 nodes with 48 cores, 180GB of RAM available.
    - [AMD EPYC:](https://cesga-docs.gitlab.io/ft3-user-guide/other_nodes.html#amd-epyc-nodes) 18 nodes with 64 cores per node and 256GB of RAM memory.
- **viz partition:** 16 nodes with 64 cores and 246GB of RAM available and 1 [T4 GPU](https://cesga-docs.gitlab.io/ft3-user-guide/gpu_nodes.html#tesla-t4) devoted to interactive use.
- **data partition:** 2 [dtn nodes](https://cesga-docs.gitlab.io/ft3-user-guide/data_transfer.html#dtn-nodes) with 64 cores and 246GB of RAM available devoted to data transfer.

:::{note}
Automatic partition asigment will be done on short, medium, long, ondemand and requeue partitions depending on demanded resources and job characteristics. Also, to use clk or AMD EPYC nodes is necessary to add the flag -C clk or -C epyc.
:::

To see more information about partitions, you can use the `batchlim` on the command line and it will show you partition limits, QOS limits and your user and account limits.

```console
Name      | Default |  MaxNodes |     MaxTime | Shared | TotalCPUs | TotalNodes | MaxMemPerNode
----------|---------|-----------|-------------|--------|-----------|------------|--------------
ondemand  | NO      | UNLIMITED | 42-00:00:00 | NO     |     21504 |        336 | UNLIMITED MB
long      | NO      | UNLIMITED |  7-00:00:00 | NO     |     21504 |        336 | UNLIMITED MB
medium    | NO      | UNLIMITED |  3-00:00:00 | NO     |     21504 |        336 | UNLIMITED MB
requeue   | NO      | UNLIMITED |  1-00:00:00 | NO     |     21504 |        336 | UNLIMITED MB
short     | YES     | UNLIMITED |    06:00:00 | NO     |     21504 |        336 | UNLIMITED MB
viz       | NO      | UNLIMITED |    08:00:00 | NO     |       640 |         10 | UNLIMITED MB
ligo      | NO      | UNLIMITED |  2-00:00:00 | NO     |      2108 |         38 | UNLIMITED MB
otoom     | NO      | UNLIMITED |   UNLIMITED | NO     |        68 |          4 | UNLIMITED MB
```
