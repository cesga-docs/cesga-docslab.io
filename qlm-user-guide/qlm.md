% QLM User Guide documentation master file

% :title: QLM User Guide
% :section: qlm-user-guide
% :url_source: https://cesga-docs.gitlab.io/qlm-user-guide/qlm.html
% :topic: qlm
% :keywords: QLM, QUANTUM COMPUTING, SIMULATION, QUBITS, ATOS, CESGA, SSH, VPN, SCP, SFTP, QLMAAS, MYQLM, JUPYTER NOTEBOOK, REMOTE DESKTOPS, PYTHON, CERTIFICATES, ANNEX I, DOCUMENTATION, USER GUIDE, QUANTUM CIRCUITS, HYBRID ALGORITHMS, NOISE SIMULATION MODELS, HARDWARE-INDEPENDENT ENVIRONMENT.
% :content:

# QLM User Guide

QLM (Quantum Learning Machine) is a quantum computing system optimized for the simulation of quantum circuits. Our model **QLM30** which allows simulation of up to 30 qubits is developed by ATOS. This system is designed to help researchers and businesses learn and experiment with quantum computers, develop applications and algorithms without having to wait for physical quantum machines to become available.

:::{Note}
Access to QLM should be requested in advance by providing your username and a brief description of the work to be carried out. This information must be sent to **aplicaciones@cesga.es**
:::

## System description

QLM is a computing system optimized for the **simulation of quantum circuits**. The system includes both **dedicated hardware** (48 cores with hyperthreading enabled, generating 96 virtual cores and 1510GB of RAM) and **simulation software** (QLM PowerAccess). The latest version of the installed software is 1.7.1. The Atos Quantum Learning Machine is a complete device with the following features:

- Provides an easy quantum programming language, a set of libraries and Jupyter Notebooks.
- Allows programming and execution of hybrid (quantum-classical) algorithms.
- Supports other existing environments to leverage algorithms developed with other environments.
- Supports real noise simulation models on different hardware.
- Allows simulation of different technologies through its hardware-independent environment with good performance.

## System connection

Connection to the **QLM node** can be made through an ssh client using the following address: `10.120.6.44`. **The CESGA VPN must be active**. If you are not familiar with the use of the VPN, please visit [how to connect](https://cesga-docs.gitlab.io/ft3-user-guide/how_to_connect.html)  section. You can also connect to FinisTerrae III and access QLM from there. To do so, execute `ssh qlm`.

A username and password will be required, so, if you don't have and account on CESGA **you must request it in advance**. If you are already a CESGA user, you should **request access** by providing your username and a brief description of the work to be carried out and send this to **aplicaciones@cesga.es**

## Data transfer

For transferring files and data to or from the QLM, an **scp or sftp client should be used.**

## Storage, directories and file system

The QLM does not share file systems with the other computing systems at CESGA. Each user has their own user directory on the QLM, which includes a **qlm_notebooks directory** with some example notebooks and documentation.

## System use: Remote usage (recommended)

Calculations or simulations can be sent to the QLM from the outside through the **QLMaas service**. This guide shows its usage from FinisTerrae III, although it can be used equivalently from a personal desktop computer, with the CESGA VPN up and running and with the myqlm and qlmaas libraries installed.

**This form of remote usage is recommended as it avoids conflicts between users during execution.** The executions are performed in asynchronous mode, through a FIFO queue that controls the number of cores required by each program, allowing a more effective and concurrent execution among several users. It adds a little overhead to the request (around 4 seconds), which is noticeable especially in small programs, but as mentioned before, it is the recommended way of execution.

To start using it on FinisTerrae III, the latest installed version of myqlm must be loaded: `module load cesga/2020 gcc/system myqlm/1.7.3-python-3.9.9`

Now you can start programming the executions. Simply use the qlmaas package instead of the qat package. To make this direct and automatic substitution, it is necessary to create a local configuration file, for which you should use the following script. Please note that this step is only necessary once.

```
from qat.qlmaas import QLMaaSConnection
conn = QLMaaSConnection(hostname="10.120.6.44", check_host=False)
conn.create_config()
```

Instead of the IP address, you can also use "qlm" from within FinisTerrae III. The default authentication method is user/password. **These are the same credentials you would use to connect to QLM via ssh.** You only need to answer the questions that the script will ask you. Also, instead of using this authentication method, it is also possible to authenticate using **certificates**. A description of how to do this can be found in [Annex I.](https://cesga-docs.gitlab.io/qlm-user-guide/annex1.html)

Documentation can be accessed through `https://10.120.6.44/front/`

Once the modules are loaded and the configuration file is created, you can send simulations to QLM from the local myQLM script (remember to use qlmaas instead of qat). To check that everything is correctly configured, you can use the following command (do not add the Python command before it, it runs directly): `qlmaas_test.py`

It is also possible to use notebooks in the FinisTerrae III to develop with myqlm and send to QLM through qlmaas. To launch the notebooks, you must have the previously mentioned modules loaded and use the command: `jupyter notebook`. There is more information about how to use [jupyter notebook.](https://cesga-docs.gitlab.io/ft3-user-guide/remote_desktops.html#jupyter-notebook)

If you are using [remote desktops](https://cesga-docs.gitlab.io/ft3-user-guide/remote_desktops.html), you won't need to modify the notebook connection URL. In fact, the web browser will open automatically with Jupyter listing the files in the directory from which you invoked it. **It is recommended to use remote desktops for using notebooks.**

## System use: Local usage

When connected to the system, commands can be executed locally, files can be edited, etc.

Once connected to the QLM via ssh, commands can be executed normally. For example, to run a Python script named prueba.py, execute: `python prueba.py`
Notebooks can also be used in local mode. To launch them, use: `qlm_notebooks/launch_qlm_notebooks`

Within the information displayed on the screen, search for a line similar to `http://qlm:8888/?token=42026a37dfa1ecb01742f7b8e5928c1255281178b93d260g`. You must take into account that the token ID will be different each time.

This link should be copied and pasted into your web browser. **If accessing from outside FinisTerrae III, replace qlm with 10.120.6.44.** It should look something like this: `http://10.120.6.44:8888/?token=42026a37dfa1ecb01742f7b8e5928c1255281178b93d260g` A window like this should appear:

:::{figure} _static/screenshots/qlm.png
:align: center
:width: 500px
:::

You have access to the documentation and some sample notebooks. To create and view your own notebooks, you need to replace the "?token" part of the previous URL with "tree". For example, the URL for our example would be: `http://10.120.6.44:8888/tree`

The directory within your user account whose contents are being listed is: `$HOME/qlm_notebooks/notebooks_1.7.1`

Here, you can create your own notebooks, open others that you have copied, use any of the example notebooks provided (in notebooks or notebooks/tutorials), which can be modified, copied or reused, etc.

To access this window within the "notebooks" subdirectory, you can simply click on "notebooks" in the initial welcome window and then select File > Open.

## Useful commands: qlmaas_prompt.py

It is a command-line interface that allows us to view the status of our jobs, cancel them, delete them, etc., as well as perform other configuration tasks. It accepts the parameter `-hostname` to specify the name of the host, but whether we are within QLM or accessing it remotely and have the qlmaas configuration file (created with the script in the previous section), this parameter is not necessary. It will ask for login and password, unless we have configured access with a certificate (see [Annex I](https://cesga-docs.gitlab.io/qlm-user-guide/annex1.html) ).

Example of usage:

```
$ qlmaas_prompt.py
Username:
Password:
> ls
job id status submission date nb. qubits queue position
-----------------------------------------------------------------------------
Job3473 done 2022-01-20 07:59:44.260534 4 None
Job2898 done 2022-01-19 10:51:57.770131 4 None
Job2894 done 2022-01-19 08:40:46.873866 4 None
```

The command `ls` lists queued, running or finished Jobs. Other commands include: cancel, delete, config, etc. To see the list of commands, type `?`. To exit this command interpreter, use the `quit` command.

```
> ?
Unsupported command ?
List of command:
ls List all jobs
cancel Cancel one or several jobs
delete Delete one or several jobs
dl Try and retrieve a job result
plugins List all possible plugins
qpus List all possible qpus
config Display or update the current server-side configuration file
manage Change password, certificate or DN used to login
quit Exits the sesión
```

## Useful commands: qlmaas_test.py

This command is used to verify that everything is working correctly. It launches a simple 2-qubit circuit (Bell pair circuit) using qlmaas, and informs at the output whether the test has been successful or not. It can be invoked for both local and remote execution (it will ask for user and password unless we have configured access with a certificate). Example:

```
qlmaas_test.py
Remote QPU successfully created
Submitted a new batch: Job3532
Bell pair circuit successfully submitted to the remote QPU
Result downloaded:
-> |00> - 0.4999999999999999
-> |11> - 0.4999999999999999
Test successful
```
