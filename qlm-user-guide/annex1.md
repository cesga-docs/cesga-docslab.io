(qlm_user_guide_documentation_annex_i)=

% :title: **Annex I:** Switch to certificate-based authentication for QLMAAS
% :section: qlm-user-guide
% :url_source: https://cesga-docs.gitlab.io/qlm-user-guide/annex1.html
% :topic: annex1
% :keywords: CERTIFICATE-BASED AUTHENTICATION, QLMAAS, SSL, OPENSSL, RSA, PYTHON, QLMAASCONNECTION, CONFIG.INI, AUTHENTICATION, PASSWORD, USERNAME, KEY.PEM, CERT.PEM, KEY2.PEM, CHMOD, SSH, FINISTERRAE III, MYQLM MODULE, QLM MACHINE, DISTINGUISHED NAME, DN, PEM PASS PHRASE, PASSPHRASE, CONNECTION, HOSTNAME, PORT, TIMEOUT, CHECK_HOST, CERTIFICATE, KEY
% :content:

# **Annex I:** Switch to certificate-based authentication for QLMAAS

If you want to send jobs to the QLMaaS server without the need to add the username and password for each connection, it is necessary to switch to certificate-based authentication, which can be self-generated.

:::{Note}
This procedure works both on FinisTerrae III with the myqlm module loaded and on the QLM machine. It would also work on your QLMaaS installation at your workplace.
:::

To change to certificate-based authentication follow these steps:

1. Generate an SSL certificate with the following command: `openssl req -x509 -newkey rsa:4096 -keyout $HOME/.qlmaas/key.pem -out $HOME/.qlmaas/cert.pem -sha256 -days 720`

:::{Warning}
If this command fails, you should create the .qlmaas directory *before* executing it.
:::

When executed, it will report the progress and at some point, it will ask for information. At a minimum, you should add the country and the name.

```
Generating a RSA private key
    writing new private key to 'key.pem'
Enter PEM pass phrase:
Verifying - Enter PEM pass phrase:
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
[Server]
-----
Country Name (2 letter code) [AU]:ES
State or Province Name (full name) [Some-State]:CORUNA
Locality Name (eg, city) []:SANTIAGO
Organization Name (eg, company) [Internet Widgits Pty Ltd]:CESGA
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:Name
Email Address []: Email
```

2. You can leave the passphrase that was generated, but in that case, when connecting to QLMaaS, it will ask for that passphrase, so it is better to remove it executing: `openssl rsa -in $HOME/.qlmaas/key.pem -out $HOME/.qlmaas/key2.pem`
3. Register the new certificate on the server by running the following Python script:

```
#!/opt/cesga/2020/software/Core/python/3.9.9/bin/python3.9
# -*- coding: utf-8 -*-
import os
Home=os.getenv('HOME')
from qat.qlmaas import QLMaaSConnection
connection = QLMaaSConnection("qlm",authentication="password", check_host=False)
connection.save_certificate(Home+'/.qlmaas/cert.pem')
```

4. Edit the QLMaaS configuration file to add SSL as the default authentication system. The file \$HOME/.qlmaas/config.ini should look like this:

```
[Server]
hostname=qlm
port=443
authentication=ssl
check_host=False
timeout=600
[SSL]
certificate=~/.qlmaas/cert.pem
key=~/.qlmaas/key2.pem
```

By doing this, when making the connection, it will attempt SSL authentication through the certificate. If you want to go back to using authentication via username/password at any time, there are two possibilities:

1. For a specific occasion, when creating the connection, add that the authentication is by username/password: `connection = QLMaaSConnection("qlm",authentication="password")`
2. If it's a permanent change, switch the authentication key in the config.ini file to "password". If you want to use SSL authentication only for a specific occasion, do not change the default authentication in the config.ini file (it should be set to "password"), and indicate in the connection that you want to authenticate via SSL.

```
Import os
Home=os.getenv('HOME')
Connection=QLMaaSConnection('qlm',authentication='ssl',certificate=Home+'/.qlmaas/cert.pem',key=Home+'/.qlmaas/key2.pem')
connection.create_config()
```

5. Test that the change works by running the following command, which launches a small program on the server: `qlmaas_test.py`

Once the configuration has been tested, it can be deployed on other machines by simply copying the config.ini, key2.pem, and cert.pem files to the .qlmaas directory of the corresponding machine. Make sure that after the copy, the key is correctly protected on the new machine. For example, by running: `chmod 600 $HOME/.qlmaas/key2.pem`
