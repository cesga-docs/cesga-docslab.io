(qmio_faq)=

% :title: F.A.Q.
% :section: qmio-user-guide
% :url_source: https://cesga-docs.gitlab.io/qmio-user-guide/FAQ.html
% :topic: FAQ
% :keywords: QMIO, MODULE, DOCUMENTATION, PIP, INSTALL, DEPENDENCIES, PYZMQ, QISKIT, BACKENDS, TOOLS, VIRTUAL ENVIRONMENT
% :content:

# F.A.Q.

- Where can I look for the qmio module documentation?
  TBD
- How can I mix this with my already deployed user virtual environment?
  `pip install qmio`
- What are the qmio dependencies?
  Just `pyzmq`
- How do I use qmio-tools to interact with qmio qiskit backends?
  TBD
