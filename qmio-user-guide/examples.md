(qmio_examples)=

% :title: Examples
% :section: qmio-user-guide
% :url_source: https://cesga-docs.gitlab.io/qmio-user-guide/examples.html
% :topic: examples
% :keywords: SLURM, BASH, PYTHON, QULACS, GCC, MPIRUN, OMP_NUM_THREADS, QULACS_NUM_THREADS, OMP_PROC_BIND, MODULE, LMOD.SH, SCONTROL, NUMACTL, NODELIST, NODEFILE, RM, SH, ENV, QMIO-RUN, QMIORUNTIMESERVICE, CIRCUITS, BELL, BACKEND, NAME, QPU, SHOTS, REPETITION_PERIOD, OPTIMIZATION, RES_FORMAT
% :content:

# Examples

## ilk - Submission template

```bash
#!/bin/bash
#
#    Modify this to your needs
#
#SBATCH -p ilk
#SBATCH -o <output/file/path>
#SBATCH -N 2
#SBATCH --ntasks-per-node=1
#SBATCH -c 64
#SBATCH -t 5:0:0
#SBATCH --mem-per-cpu=15G

export SLURM_WHOLE=1
export OMP_NUM_THREADS=64
export QULACS_NUM_THREADS=64
export OMP_PROC_BIND=True

module load gcc
module load qulacs/0.6.3-python-3.9.9-mpi

mpirun -npernode ${SLURM_NTASKS_PER_NODE} python qulacs_script.py
```

## a64 - Submission template

```bash
#!/bin/bash
#
#    Plantilla. no utilizar sin retocar
#
#SBATCH -p a64
#SBATCH -N 2                      # Numero de nodos
#SBATCH --tasks-per-node=1        # Número de tareas por nodo
#SBATCH -c 48                     # Número de hilos por tarea
#SBATCH -t 0:10:0                 # Time limit
#SBATCH --mem-per-cpu=600M        # Memoria por cpu

source /etc/profile.d/lmod.sh

export OMP_NUM_THREADS=48
export QULACS_NUM_THREADS=48

module load qulacs-hpcx

scontrol show hostnames
echo $SLURM_JOB_NODELIST
nodelist=$(scontrol show hostname $SLURM_NODELIST)
printf "%s\n" "${nodelist[@]}" > nodefile

# Casos grandes se benefician de
# export OMP_PROC_BIND=TRUE
# numactl -N 0-3 <antes de la llamada de python>

mpirun -npernode ${SLURM_NTASKS_PER_NODE} -hostfile nodefile python /path/to/qulacs_script.py
rm nodefile
```

## qpu

### Submission template

```bash
#!/usr/bin/env sh

#SBATCH -p qpu
#SBATCH --mem 4G
#SBATCH -t 00:10:00

module load qmio-run

python user_test-0.py
```

### Python Simple execution

```python
from qmio import QmioRuntimeService
from qmio.circuits import bell as circuit

service = QmioRuntimeService()

shots = 100                       # Only arg requiered
repetition_period = 500*10**-6    # If None will pick internal default
optimization = 0                  # Default value
res_format = "binary_count"       # Default value

# Recommended usage. Context controled
with service.backend(name="qpu") as backend:
        result = backend.run(circuit=circuit, shots=shots, repetition_period=repetition_period, optimization=optimization, res_format=res_format)

print(result)
```
