(ft3_system_use)=

% :title: System use
% :section: qmio-user-guide
% :url_source: https://cesga-docs.gitlab.io/qmio-user-guide/system_use.html
% :topic: system_use
% :keywords: SLURM, BATCH MODE, INTERACTIVE SESSIONS, PARTITIONS, RESOURCE MANAGEMENT, SOFTWARE MANAGEMENT, LMOD, MODULES, PIP, CONDA ENVIRONMENTS, QMIO, QUANTUM COMPUTER, ARM NODES, X86 NODES, LOGIN NODES, CLUSTER, COMPUTE, SALLOC, SRUN, SINFO, SQSTAT, SCONTROL, BATCHLIM
% :content:

# System use

For a more in depth use of the system, we first need to make us more familiar with system infrastructure, specifically resource management and software management.

## Resource Management - SLURM

As any other slurm managed cluster you have some different possibilities to execute jobs in it. The main one is to work in batch mode, wich means to submit jobs to the queue. Another option is to work interactively. This can be done through the command `compute`, with this command you’ll be provided with an interactive session in x86 nodes dedicated to this job in the ilk{sub}`interactive`partition. If you would like to use an interactive session into a64 nodes, you’ll need to use another command. There is not any interactive dedicated node in this partition, so you might need to wait in the queue for resources depending on node availability. `salloc -p a64 --mem-per-cpu=600M -t 5:0:0 -c 48 srun -c 48 --pty --preserve-env /bin/bash -i`.

### Partitions

- ilk

  - ilk{sub}`interactive`

- a64

- qpu

### Usefull commands

The best way to extend the information is to ask directly to the cluster with commands as the following ones. `batchlim`, `sqstat`, `scontrol show partition`, `sinfo`.

Example with batchlim.

```bash
$ batchlim

*******************
 Partition limits:
*******************
 PartitionName | Default |  MaxNodes |     MaxTime | Shared    | TotalCPUs | TotalNodes |  DefMemPerCPU |  MaxMemPerNode
---------------|---------|-----------|-------------|-----------|-----------|------------|---------------|----------------
qpu            | NO      | UNLIMITED |   UNLIMITED | EXCLUSIVE |        64 |          1 |        1024MB |   UNLIMITED MB
ilk_interactive | NO      | UNLIMITED |   UNLIMITED | NO        |       128 |          2 |          ---- |   UNLIMITED MB
ilk            | YES     | UNLIMITED |   UNLIMITED | NO        |      1280 |         20 |          ---- |   UNLIMITED MB
a64            | NO      | UNLIMITED |   UNLIMITED | NO        |       768 |         16 |          ---- |   UNLIMITED MB

*************
 QOS limits:
*************
        Name   Priority       GrpTRES       MaxTRES     MaxWall MaxJobsPU     MaxTRESPU MaxSubmit
------------ ---------- ------------- ------------- ----------- --------- ------------- ---------
   ilk_short         50                    cpu=1408    06:00:00       100      cpu=1408       200
  ilk_medium         40                    cpu=1408  3-00:00:00       100      cpu=1408       125
    ilk_long         30       cpu=640       cpu=640  7-00:00:00        30       cpu=640        30
ilk_ondemand         10       cpu=320       cpu=320 42-00:00:00        10       cpu=320        10
   a64_short         50                     cpu=768    06:00:00       100       cpu=768       200
  a64_medium         40                     cpu=768  3-00:00:00       100       cpu=768       125
    a64_long         30       cpu=288       cpu=288  7-00:00:00        30       cpu=288        30
a64_ondemand         10       cpu=144       cpu=144 42-00:00:00        10       cpu=144        10
         qpu        100                      node=1    02:00:00         1        cpu=64         5
ilk_interac+        100                      node=1    08:00:00         2 cpu=8,mem=12+         2
a64_interac+        100                      node=1    08:00:00         2 cpu=8,mem=50+         2

...
```

## Software Management - Modules

To be able to use different software options and versions we have configured `lmod` package. We use the `module` command to ask for specific software.

### x86 module tree

This module tree is loaded by default in login nodes and any other node of the system.

### a64 module tree

ARM based nodes do not share the same module tree so users need to repoint to the correct module tree using `source /etc/profile.d/lmod.sh`.

### Qmio Module

To load the python module to interact with the quantum computer you have two options:

1. `module load qmio-run` / `module load qmio-tools`
   This implies that you are going to use other modules compatible with python versions available in the module system. This doesn’t integrate well with per-user enviroments like conda enviroments.
2. `pip install qmio`
   The qmio package is available from pipy so you can install it from there into your enviroment. You can create your own enviroment or install it user-wide with `pip install --user qmio`. Check [pipy-qmio](https://pypi.org/project/qmio/).

If you use the second method, you have to handle the update by yourself with `pip install -U qmio`. We will notify you whenever an update is ready.
