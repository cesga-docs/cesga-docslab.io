% :title: Additional references
% :section: qmio-user-guide
% :url_source: https://cesga-docs.gitlab.io/qmio-user-guide/additional_reference.html
% :topic: additional_reference
% :keywords: QMIO, CHEAT, SHEET, API, REFERENCE
% :content:

(qmio_additional_reference)=
# Additional references

- [Qmiotools API Reference](https://gomeztato.github.io/qmiotools/)
- [Qmio tutorial](https://github.com/javicacheiro/qmio-tutorial-ibergrid)
- [Qmio Cheat Sheet](_static/cheat-sheet.pdf)

