Dissemination and publicity
===========================

For any user which may conduct any research project using the CESGA's services, our center must be properly cited as a collaborating center and mentioned in the acknowledgments section. 

We offer a version in Spanish, Galician and English that should be used. While modifications on the text are permitted, it is essential that the **minimum information** outlined below remains intact:

**English version:**

> This research project was made possible through the access granted by the Galician Supercomputing Center (CESGA) to its Qmio quantum computing infrastructure with funding from the European Union, through the Operational Programme Galicia 2014-2020 of ERDF_REACT EU, as part of the European Union's response to the COVID-19 pandemic.

**Galician version:**

> Este proxecto de investigación foi posible grazas ao acceso garantido polo Centro de Supercomputación de Galicia a súa infraestructura de computación cuántica Qmio, a cal foi financiada pola Unión Europea, a través do Programa Operativo Galicia 2014-2020 de FEDER_REACT EU, como parte da resposta da Unión Europea á pandemía COVID-19.


**Spanish version:**

> Este trabajo de investigación se desarrolló gracias al acceso concedido por el Centro de Supercomputación de Galicia (CESGA) a su infraestructura de computación cuántica Qmio, la cual fue financiada por la Unión Europea, a través del Programa Operativo Galicia 2014-2020 de FEDER_REACT EU, como parte de la respuesta de la Unión Europea a la pandemía COVID-19.
