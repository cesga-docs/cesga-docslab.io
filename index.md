% CESGA documentation master file, created by

% :title: Indices and tables
% :section: home
% :url_source: https://cesga-docs.gitlab.io/index.html
% :topic: index
% :keywords: CESGA, TECHNICAL DOCUMENTATION, NEWS, USER GUIDE, CLOUD OPENSTACK, DTN, BIGDATA, QRNG, QLM, FT2, FT3, QMIO
% :content:

# CESGA Technical Documentation

```{toctree}
news.rst
```

```{toctree}
ft3-user-guide/index
```

```{toctree}
cloud-openstack/index
```

```{toctree}
dtn-user-guide/index
```

```{toctree}
bigdata-user-guide/index
```

```{toctree}
qrng/index
```

```{toctree}
qlm-user-guide/index
```

```{toctree}
ft2-user-guide/index
```

```{toctree}
qmio-user-guide/index
```

## Indices and tables

- {ref}`genindex`
- {ref}`search`
