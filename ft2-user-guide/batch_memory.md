(ft2_batch_memory)=

% :title: Memory
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/batch_memory.html
% :topic: batch_memory
% :keywords: SLURM, MEMORIA, NODOS, CORES, CPUS, CPU, OPCIONES, SBATCH, PARALELIZAR, SCRIPT
% :content:

# Memory

El slurm por defecto reserva 5GB de memoria por cada core tanto en los
nodos estándar como en el fat node, pero en el caso de querer más
memoria hay que especificarlo con las opciones **\*--mem\*** y
**\*--mem-per-cpu\***, y también sería aconsejable utilizar estas opciones
en caso de necesitar menos para así dejar libre la que no se va a usar
para otros trabajos. En el caso de un trabajo que se vaya a ejecutar en
un nodo en exclusiva, por defecto se asigna toda la memoria del nodo
(120GB en los nodos estándar).

A continuación se mostrarán unos cuantos ejemplos de la distribución de
la memoria.

Se procede a enviar un trabajo que no necesita de paralelizar y que
requiere de 6GB de memoria, con lo que se especificaría de la siguiente
forma:

**\*\$ sbatch -n 1 --mem=6GB script.sh\***

Si lo que queremos es más de un core para una tarea, y que cada core
tenga 20GB de memoria habría que hacer:

**\*\$ sbatch -n 1 --cpus-per-task=2 --mem-per-cpu=20GB script.sh\***
