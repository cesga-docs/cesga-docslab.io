(ft2_envs_modules)=

% :title: Environment modules (Lmod)
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/env_modules.html
% :topic: env_modules
% :keywords: AREA_COMPILER, AREA_MPI, AREA_BIOINFORMATICS, AREA_CHEMISTRYANDMATERIALS,AREA_EDITOR, AREA_MACHINELEARNING, AREA_MULTIPHYSICSANDCFD, AREA_MATHLIBRARY, AREA_PROFILING, AREA_SCIENTIFICANALYSIS, AREA_SIMULATION, AREA_SOFTWAREMANAGEMENT, AREA_TOOLS, AREA_VISUALIZATIONANDDATAFORMATS, MODULE, LMOD, INTEL, IMPI, GCC, GLIBC

>

# Environment modules (Lmod)

El comando **\*module \***es el comando central que permite la
configuración y uso de las aplicaciones proporcionadas por el CESGA. Con
él se fija el entorno de variables apropiado para una aplicación
determinada independiente de la shell de usuario. Las aplicaciones
disponibles están jerarquizadas y están ligadas a la carga previa de una
combinación concreta de dependencias, actualmente el compilador con la
que fue compilada y, en aplicaciones MPI, la versión de MPI usada en su
compilación. Esto intenta minimizar problemas de incompatibilidades
entre las distintas librerías de los compiladores/MPI.

Módulos disponibles:

**\*\$ module avail\***

{{ image2 }}

…

{{ image3 }}

…

{{ image4 }}

…

{{ image5 }}

…

{{ image6 }}

Inicialmente, los únicos módulos disponibles son los pertenecientes a la
sección “Core” donde se localizan compiladores y aplicaciones cuyo uso
es independiente de estos (aplicaciones compiladas con la versión
gcc/glibc por defecto usado como base del resto de compiladores y por
tanto compatibles entre sí) .

Adicionalmente las aplicaciones están agrupadas en las siguientes áreas:

- Comp:Compiler (key: **area_compiler**)
- MPI:MPI library (key: **area_mpi**)
- Bio:Bioinformatics (key: **area_bioinformatics**)
- Chem: Chemistry and Materials (key: **area_chemistryandmaterials**)
- Ed: Editor (key: **area_editor**)
- ML: Machine Learning (key: **area_machinelearning**)
- MPCFD: Multiphysics and CFD (key: **area_multiphysicsandcfd**)
- Math: Math Library (key: **area_mathlibrary**)
- Prof: Profiling (key: **area_profiling**)
- ScA:Scientific Analysis (key: **area_scientificanalysis**)
- Sim:Simulation (key: **area_simulation**)
- Soft: Software Management (key: **area_softwaremanagement**)
- Tool: Tool (key: **area_tools**)
- VisF: Visualization and data formats (key:
  **area_visualizationanddataformats**)

Mediante la clave asociada a cada área es posible buscar las
aplicaciones agrupadas en ella mediante la opción “key” del comando
module:

{{ image7 }}

En la sección “ORGS” aparecen las organizaciones (árboles de módulos
independientes) a las que el usuario tiene acceso. Por defecto las
organizaciones disponibles son:

```{eval-rst}
+--------------+----------------------------------------+
| **ORGS**     | **Compilador/glibc por defecto**       |
+--------------+----------------------------------------+
| cesga/2018   | GCC 6.4.0 glibc 2.28                   |
+--------------+----------------------------------------+
| cesga/2020   | Gentoo Prefix: GCC 10.1.0 glibc 2.31   |
+--------------+----------------------------------------+
```

Cargar un módulo para configurar el entorno para el uso de una
aplicación:

**\*\$ module load intel\***

{{ image8 }}

Al cargar el módulo correspondiente a los compiladores de intel la
sección de aplicaciones correspondiente a estos compiladores está
disponible para la carga. Aparecen los módulos de MPI disponibles. Si se
carga un módulo de MPI, por ejemplo **\*impi\***, aparece disponible la
sección de aplicaciones disponibles para la combinación compiladores
(intel) / MPI (impi).

{{ image9 }}

Descargar un módulo/s:

**\*\$ module unload package1 package2 …\***

Módulos cargados actualmente:

**\*\$ module list\***

{{ image10 }}

En un momento dado puede ser interesante cambiar los compiladores en
uso:

**\*\$ module swap intel gcc\***

Con este comando se cambiarán todas las librerías/aplicaciones
dependientes del compilador. Si no existieran las aplicaciones para el
compilador los módulos correspondientes se marcan con inactivos,
inhabilitando su uso hasta que se haga el cambio de nuevo al compilador
original:

{{ image11 }}

Los módulos contienen pequeñas guías de uso de las correspondiente
aplicaciones/librerías. Estás se obtienen mediante el comando:

**\*\$ module help packageName\***

Hay que tener en cuenta que para obtener la ayuda de un módulo
determinado mediante este comando este debe listarse mediante el comando
“modula avail”, es decir, las dependencias en su jerarquía cargadas.

> {{ image12 }}

Para obtener ayuda sobre el propio uso del comando module:

**\*\$ module help\***

Para ver todas las aplicaciones/librerías disponibles independientemente
de los compiladores/MPI de los que dependan se usa el comando:

**\*\$ module spider\***

Sin ningún argumento este comando proporciona la lista completa de
aplicaciones/librerías disponibles.

Es posible buscar mediante este comando si una aplicación está
disponible:

{{ image13 }}
