(ft2_annex_2_ib)=

% :title: Annex II: Infiniband network
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/annex_2_ib.html
% :topic: annex_2_ib
% :keywords: INFINIBAND, MELLANOX, FAT-TREE, FINIS-TERRAE-II
% :content:

# Annex II: Infiniband network

Todos los nodos de Finis Terrae II están conectados a una red infiniband
Mellanox Infiniband <mailto:FDR@56Gbps> con topología Fat-tree

{{ image20 }}
