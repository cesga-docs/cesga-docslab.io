(ft2_annex_4_dependencies)=

% :title: Annex IV: Job dependencies
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/annex_4_dependencies.html
% :topic: annex_4_dependencies
% :keywords: JOB, DEPENDENCIES, HOLD, RELEASE, SCONTROL, DEPENDENCY, LIST, TYPE, JOB_ID, AFTER, AFTERANY, AFTERNOTOK, AFTEROK, SINGLETON
% :content:

# Annex IV: Job dependencies

A la hora de enviar los trabajos podemos indicar que el trabajo se quede
en espera hasta que nosotros decidamos o bien indicarle que el trabajo
depende de otros trabajos que ya están en la cola. Las opciones a usar
para estas características son las siguientes:

- **\*-H, --hold\***: Indica que el trabajo será enviado en un estado
  retenido y deberá ser liberado por el usuario para ponerse en cola
  utilizando el comando “**\*scontrol release \<job_id>\***”. También
  es posible poner un trabajo en este estado una vez ya está en cola
  con el comando “**\*scontrol hold \<job_id>\***”.

- **\*-d, --dependency=\<dependency list>\***: Se aplaza el inicio del
  trabajo hasta que se cumplan todas las dependencias.
  *\<dependency_list>* es de la forma
  *\<type:job_id\[:job_id\]\[,type:job_id\[:job_id\]\]>*. Varios trabajos
  pueden compartir la misma dependencia y estos trabajos pueden incluso
  pertenecer a usuarios diferente. Este valor puede cambiarse después
  del envío del trabajo con el comando *scontrol*.

  - **\*after:job_id\[:jobid...\]\***: El trabajo puede ejecutarse
    : después de que los trabajos indicados hayan comenzado su
      ejecución.
  - **\*afterany:job_id\[:jobid...\]\***: El trabajo puede ejecutarse
    : después de que los trabajos indicados hayan finalizado.
  - **\*afternotok:job_id\[:jobid...\]\***: El trabajo puede ejecutarse
    : después de que los trabajos indicados hayan finalizado en
      error.
  - **\*afterok:job_id\[:jobid...\]\***: El trabajo puede ejecutarse
    : después de que los trabajos indicados hayan finalizado
      correctamente.
  - **\*singleton\***: El trabajo puede ejecutarse después de que
    : cualquier trabajo enviado previamente con el mismo nombre haya
      finalizado.
