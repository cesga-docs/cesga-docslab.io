(ft2_system_use)=

% :title: System use
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/system_use.html
% :topic: system_use
% :keywords: SYSTEM USE, INTERACTIVE USE, REMOTE DESKTOP, DEDICATED INTERACTIVE NODES, QUEUING SYSTEM, RESOURCES, INTERACTIVITY, SIMULATIONS, COMPILATIONS, FILES, MEMORY, BROWSER, URL, X11, APPLICATIONS, SESSIONS, CORES, TIME, GUARANTEES
% :content:

# System use

There are four ways to use the equipment, depending on the type of
resources and interactivity that are needed:

- **Interactive use**
  : when connecting to the computer you have access to
    limited and shared resources, invalid to perform
    simulations, but to make small compilations, work with
    files and interact with the system. The maximum memory available
    is 8GB
- **Remote Desktop**
  : for remote desktop access you can
    access through a browser to the following URL:
    [https://portalusuarios.cesga.es/tools/remote_vis](https://portalusuarios.cesga.es/tools/remote_vis)
    through which you have access to a complete graphic system X11
    to display and access those applications that
    have a graphical interface. However, it will not be possible
    perform long simulations or use more than 32GB of memory.
- **Dedicated interactive nodes**
  : it is possible to start sessions
    interactive but with dedicated resources using the command
    “compute” (for more information it can be used with the option
    --help) from the node the user connects to. Still, the
    maximum resources available are 24 cores and 5GB of memory per core and
    up to 8 hours.
- **Using the queuing system**
  : to perform simulations
    with multiple cores and large amounts of memory and time, it is
    necessary to use the queuing system, which guarantees the resources
    assigned to each user. The following section describes how
    queuing system is used.

:::{figure} _static/screenshots/system_use.png
:align: center
:::
