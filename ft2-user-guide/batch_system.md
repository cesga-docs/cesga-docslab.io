(ft2_batch_system)=

% :title: Batch system
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/batch_system.html
% :topic: batch_system
% :keywords: SLURM, QUEUING SYSTEM, BATCH SYSTEM, TUTORIALS, GUIDES, QUICKSTART, USER GUIDE, SLURM TUTORIALS, USEFUL LINKS, CESGA, PORTALUSUARIOS, BATCH_BASIC_COMMANDS, BATCH_PARTITIONS, BATCH_QOS, BATCH_EXAMPLES, BATCH_MEMORY, BATCH_JOB_MONITORING, BATCH_STDOUT_ERR, BATCH_EMAIL_WARNING
% :content:

# Batch system

The queuing system is based on SLURM. For more information you can
consult the guides and tutorials available at:

Quickstart user guide:
[http://www.schedmd.com/slurmdocs/quickstart.html](http://www.schedmd.com/slurmdocs/quickstart.html)

Slurm Tutorials:
[\*http://www.schedmd.com/slurmdocs/tutorials.html\*](http://www.schedmd.com/slurmdocs/tutorials.html)

Useful links:
[\*https://portalusuarios.cesga.es/info/links\*](https://portalusuarios.cesga.es/info/links)

```{toctree}
batch_basic_commands
batch_partitions
batch_qos
batch_examples
batch_memory
batch_job_monitoring
batch_stdout_err
batch_email_warning
```
