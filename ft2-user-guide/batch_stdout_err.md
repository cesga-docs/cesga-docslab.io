(ft2_batch_stdout_err)=

% :title: Standart output and error
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/batch_stdout_err.html
% :topic: batch_stdout_err
% :keywords: SLURM, SBATCH, STDOUT, STDERR, JOB ARRAY, ERROR, OUTPUT, FILE PATH, DIRECTORY
% :content:

# Standart output and error

Slurm combina por defecto la salida estándar y de error en un único
fichero llamado **\*slurm-\<job_number>.out\*** o en
**\*slurm-\<job_number>\_\<array_index>.out\*** si se trata de un job
array. Por defecto estos ficheros se guardan en el directorio de envío.
Se puede modificar este comportamiento con las siguientes opciones del
comando **\*sbatch\***:

**\*--error /path/to/dir/filename \***

**\*--output /path/to/dir/filename\***
