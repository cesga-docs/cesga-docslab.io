% CESGA FT 2 User's Guide documentation master file

% :title: Shutdown systems: FinisTerrae II
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/index.html
% :topic: index
% :keywords: CESGA, FINISTERRAE II, SHUTDOWN SYSTEMS, USER'S GUIDE, DOCUMENTATION, MASTER FILE, OVERVIEW, USERS PORTAL, HOW TO CONNECT, DATA TRANSFER, STORAGE, SYSTEM USE, BATCH SYSTEM, USEFUL OPTIONS, JOBS STATES, COMPILERS AND DEV TOOLS, ENV MODULES, SCRATCH DIRS, FAT NODE, GPU NODES, FAQS, ANNEXES
% :content:

# Shutdown systems: FinisTerrae II

```{toctree}
overview
users_portal
how_to_connect
data_transfer
storage
system_use
batch_system
useful_options
jobs_states
compilers_and_dev_tools
env_modules
scratch_dirs
fat_node
gpu_nodes
want_to_know_more
faqs
annex_1_desc
annex_2_ib
annex_3_job_arrays
annex_4_dependencies
annex_5_multiple_tasks_on_job
annex_6_binding
annex_7_lustre
annex_8_drop_caches
annex_9_containers
annex_10_cron
annex_11_remote_desktops
annex_12_job_signals
annex_13_data_transfer
annex_14_hybrid_programs
```
