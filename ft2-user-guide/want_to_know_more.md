(ft2_want_to_know_more)=

% :title: Want to know more
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/want_to_know_more.html
% :topic: want_to_know_more
% :keywords: FINIS TERRAE II, DOCUMENTACIÓN, MPI, CESGA, SISTEMAS, APLICACIONES, PORTALUSUARIOS.CESGA.ES, TALLER PARA USUARIOS, ACCESO AL SISTEMA.
% :content:

# Want to know more

En
[\*https://portalusuarios.cesga.es/info/links\*](https://portalusuarios.cesga.es/info/links)
se pueden encontrar diversos enlaces a documentación útil como por
ejemplo, la documentación (videos y presentaciones) del **\*Taller para
usuarios del Finis Terrae II\***.

Para cualquier problema con el acceso al sistema:
[\*sistemas@cesga.es\*](mailto:sistemas@cesga.es) o llamando al 981 56
98 10 y preguntando por el Departamento de Sistemas.

Para la parte de dudas de como compilar el programa o temas específicos
de MPI: [\*aplicaciones@cesga.es\*](mailto:aplicaciones@cesga.es) o
llamando al 981 56 98 10 y preguntado por el Departamento de
Aplicaciones.
