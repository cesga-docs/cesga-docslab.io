(ft2_batch_examples)=

% :title: Examples
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/batch_examples.html
% :topic: batch_examples
% :keywords: SBATCH, SRUN, SBATCH, OPENMP, MPI, TIME, HOSTNAME, JOB.SH, SCRIPT, RUN.SH, OMP, ICC, IFORT, MODULE, LOAD, INTEL, IMPI, PI3, COLA-CORTA, THINNODES, SHARED, CPU_BIND, MASK_CPU, CORES, NODES, TASKS, QOS, PARTITION, RESERVATION, RESOURCES
% :content:

# Examples

Es obligatorio especificar el tiempo máximo de ejecución con la opción
**--time=D-HH:MM:SS (ó de forma reducida -t D-HH:MM:SS)**

Utilizando srun (*no recomendable*)

**\*\$ srun -N2 -n2 -exclusive --time=00:00:10 hostname\***

Solicita dos servidores o nodos (N2) y envía dos tareas (n2) en modo
exclusivo, (-exclusive) y ejecuta el comando “**\*hostname\***”, con
un tiempo máximo de ejecución de 10 segundos. No es recomendable su uso
ya que bloquea el prompt hasta la ejecución completa del trabajo.

Utilizando sbatch

Generar un script *job.sh* con el siguiente contenido:

**\*#!/bin/sh\***

**\*#SBATCH -N 2 #(solicita dos nodos)\***

**\*#SBATCH -n 2 #(dos tareas en total)\***

**\* #SBATCH -p thinnodes #(solicita la partición específica. Pueden
solicitarse varias separadas por comas)\***

**\*#SBATCH -t 00:00:30 #(30 sec ejecución)\***

**\*srun hostname \***

Y enviar el trabajo con el comando:

**\*\$ sbatch ./job.sh\***

Las opciones de sbatch se pueden cambiar en el momento del envío. Así,
para enviar el script anterior a la partición\* cola-corta sin
cambiar el script, se ejecutaría:

**\*\$ sbatch -p cola-corta ./job.sh\***

Envío de un trabajo OpenMP

1. Compilación:

   > **\*\$ module load intel\***
   >
   > **\*\$ ifort -qopenmp -o omphello_f
   > /opt/cesga/job-scripts-examples/omphello.f\***
   >
   > **\*\$ icc -qopenmp -o omphello_c
   > /opt/cesga/job-scripts-examples/omphello.c\***

1) Script de ejecución en cola (partición por defecto thin-shared):

   > **\*\$ cat run.sh\***
   >
   > **\*#!/bin/bash\***
   >
   > **\*#SBATCH -n 1 #(una tarea en total) \***
   >
   > **\*#SBATCH -c 8 #(8 cores por tarea)\***
   >
   > **\*#SBATCH -t 00:10:00 #(10 min ejecución)\***
   >
   > **\* \***
   >
   > **\*./omphello_f\***

1. Envío a cola:

**\*\$ sbatch run.sh\***

Envío de un trabajo OpenMP a la partición shared

1. Script de ejecución en cola:

**\*\$ cat run.sh\***

**\*#!/bin/bash\***

**\*#SBATCH -n 1 #(una tarea en total) \***

**\*#SBATCH -c 8 #(8 cores por tarea)\***

**\*#SBATCH -t 00:10:00 #(10 min ejecución)\***

**\*#SBATCH -p shared\***

**\*#SBATCH --qos shared\***

> **\* \***

**\*./omphello_f\***

1. Envío a cola:

**\*\$ sbatch run.sh\***

Envío de un trabajo OpenMP a particiones exclusivas

En las particiones exclusivas la reserva mínima es un nodo completo,
esto implica la necesidad de especificar los cores concretos a usar si
no se usarán todos los cores disponibles.

1. Script de ejecución en cola:

**\*\$ cat run.sh\***

**\*#!/bin/bash\***

**\*#SBATCH -n 1 #(una tarea en total) \***

**\*#SBATCH -t 00:10:00 #(10 min ejecución)\***

**\*#SBATCH -p cola-corta,thinnodes**\*\*&#32;

**\*#--cpu_bind=verbose,mask_cpu:f0000f (ejecución en 8 cores:
0-3,20-23)\***

**\*srun \*\*\*\*--cpu_bind=verbose,mask_cpu:f0000f
\*\*\*\*./omphello_f\***

1. Envío a cola:

**\*\$ sbatch run.sh\***

Envío de un trabajo MPI

1. Compilación del programa:

   > **\*\$ module load intel impi\***
   >
   > **\*\$ mpiifort -o pi3 opt/cesga/job-scripts-examples/pi3f90.f90\***

1) Script de ejecución en cola:

   > **\*\$ cat run.sh\***
   >
   > **\*#!/bin/bash \***
   >
   > **\*#SBATCH -n 8 \***
   >
   > **\*#SBATCH --ntasks-per-node=4\***
   >
   > **\*#SBATCH -c 6\***
   >
   > **\*#SBATCH -p thinnodes\***
   >
   > **\*#SBATCH -t 00:10:00\***
   >
   > **\*module load intel impi \***
   >
   > **\*srun ./pi3\***

Se solicita la ejecución en 2 nodos, usando 8 procesos (*-n 8*) y 4
procesos por nodo (*--ntasks-per-node=4*) y dos cores por proceso (*-c
6*, en caso de que el programa pueda usar este tipo de paralelización
híbrida. Ver la siguiente sección), en total 48 cores (2 nodos). Al
solicitar más de un nodo es necesario especificar en qué partición debe
ejecutarse (*-p thinnodes*) ya que la partición por defecto admite un
uso máximo de 1 nodo.

No es necesario cargar los módulos de las aplicaciones/compiladores/mpi
dentro de los scripts de ejecución, basta con que estén cargados en el
momento de envío a cola ya que el trabajo hereda el entorno del momento
del envío. De hecho, por defecto, el directorio de trabajo será el
directorio desde donde se envía el script.

1. Envío a cola:

**\*\$ sbatch run.sh\***

Ejemplos de envíos para la ejecución usando 4 cores:

Si se quiere reservar para una tarea **4 cores para crear 4 threads**:

**\*\$ sbatch -p shared --qos=shared -n 1 -c 4 script.sh\***

Si se quiere reservar para una tarea **4 cores para crear 4 procesos**:

**\*\$ sbatch -p shared --qos=shared -n 4 script.sh\***

Si se quiere reservar para una tarea **4 cores para crear 2 procesos con
2 cores cada uno (**ejemplo híbrido con OpenMP y MPI):

**\*\$ sbatch -p shared --qos=shared -n 2 -c 2 script.sh\***

En caso de necesitar usar varios nodos se requiere el uso de las
particiones exclusivas (cola-corta,thinnodes), por ej:

**\*\$ sbatch -p thinnodes -n2 --tasks-per-node=1 script.sh\***

Se puede observar que se está especificando que se reserven 2 nodos y 2
tareas en total (procesos MPI), que en cada nodo se ejecute una tarea.

**\*\$ sbatch -n 4 --tasks-per-node=2 -p thinnodes script.sh\***

En esta caso la reserva es de 2 nodos pero se ejecutarán 4 tareas, 2 por
nodo.

Para limitar el nº de cores a usar de la reserva (nodos exclusivos) es
necesario ejecutar con srun y la opción “**\*--cpu_bind\***” adecuada
(ver apéndice VI y XIV) .

```{eval-rst}
+-------------------------------------------------------+------------------+------------------------+------------+------------------------+------------------------------------------+
| Opciones sbatch                                       | Nº tareas (-n)   | Cores por tarea (-c)   | Nº nodos   | Cores totales a usar   | Reserva de recursos (cores reservados)   |
+-------------------------------------------------------+------------------+------------------------+------------+------------------------+------------------------------------------+
| ***-p shared --qos=shared -n 1 --cpus-per-task=4***   | 1                | 4                      | 1          | 4                      | 4 (partición compartida)                 |
+-------------------------------------------------------+------------------+------------------------+------------+------------------------+------------------------------------------+
| ***-p thinnodes ***                                   | 2                | 2                      | 2          | 4                      | 48\* (partición exclusiva)               |
|                                                       |                  |                        |            |                        |                                          |
| ***-n2 --tasks-per-node=1***                          |                  |                        |            |                        |                                          |
|                                                       |                  |                        |            |                        |                                          |
| (srun--cpu\_bind=mask\_cpu:800001)                    |                  |                        |            |                        |                                          |
+-------------------------------------------------------+------------------+------------------------+------------+------------------------+------------------------------------------+
| ***-p thinnodes ***                                   | 4                | 1                      | 2          | 4                      | 48\* (partición exclusiva)               |
|                                                       |                  |                        |            |                        |                                          |
| ***-n 4 --tasks-per-node=2***                         |                  |                        |            |                        |                                          |
|                                                       |                  |                        |            |                        |                                          |
| (srun--cpu\_bind=mask\_cpu:1,800000)                  |                  |                        |            |                        |                                          |
+-------------------------------------------------------+------------------+------------------------+------------+------------------------+------------------------------------------+
```

\* en las particiones exclusivas sólo un único trabajo puede ejecutarse
en un nodo. El nº de nodos son reservados completamente
independientemente de los cores que se usen
