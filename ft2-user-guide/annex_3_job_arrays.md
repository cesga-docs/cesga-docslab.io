(ft2_job_arrays)=

% :title: Annex III: Job arrays
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/annex_3_job_arrays.html
% :topic: annex_3_job_arrays
% :keywords: SLURM, JOB ARRAYS, ENV VARIABLES,SBATCH_ARRAY_INX, SLURM_ARRAY_TASK_ID, SLURM_ARRAY_JOB_ID, SLURM_ARRAY_TASK_COUNT, SLURM_ARRAY_TASK_MAX, SLURM_ARRAY_TASK_MIN, SLURM_ARRAY_TASK_STEP, STEP FUNCTION, QOS, PARTITION, MAXARRAYSIZE
% :content:

# Annex III: Job arrays

Un job array en Slurm implica que el mismo script va a ser ejecutado “n”
veces, la única diferencia entre cada ejecución es una simple variable
de entorno, “**\*\$SLURM_ARRAY_TASK_ID”\***.

Esta opción simplifica el envío de “n” trabajos similares y le
proporciona al usuario una gestión única simulando un único trabajo pero
internamente slurm los trata como “n” trabajos independientes y así
contribuyen en todos los límites del usuario (partición y QoS).

Opción:

**-a, --array=\<indexes>**

Submit a job array, multiple jobs to be executed with identical
parameters. The indexes specification identifies what array index values
should be used. Multiple values may be specified using a comma separated
list and/or a range of values with a "-" separator. For example,
"**--array=0-15**" or "**--array=0,6,16-32**". A **step function** can
also be specified with a suffix containing a colon and number. For
example, "**--array=0-15:4**" is equivalent to "**--array=0,4,8,12**". A
**maximum number of simultaneously running tasks** from the job array
may be specified using a "%" separator. For example "**--array=0-15%4**"
will limit the number of simultaneously running tasks from this job
array to 4. The minimum index value is 0. the maximum value is one less
than the configuration parameter MaxArraySize.

**INPUT ENVIRONMENT VARIABLES**

SBATCH_ARRAY_INX: Same as -a, --array

**OUTPUT ENVIRONMENT VARIABLES**

SLURM_ARRAY_TASK_ID : Job array ID (index) number.

SLURM_ARRAY_JOB_ID : Job array's master job ID number.

SLURM_ARRAY_TASK_COUNT: Total number of tasks in a job array.

SLURM_ARRAY_TASK_MAX: Job array's maximum ID (index) number.

SLURM_ARRAY_TASK_MIN: Job array's minimum ID (index) number.

SLURM_ARRAY_TASK_STEP: Job array's index step size.

**Ejemplo: /opt/cesga/job-scripts-examples/Job_Array.sh**
