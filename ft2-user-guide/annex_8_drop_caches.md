(ft2_drop_caches)=

% :title: Annex VIII: Drop caches
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/annex_8_drop_caches.html
% :topic: annex_8_drop_caches
% :keywords: THINNODES, DROP, CACHES, RENDIMIENTO, PARTICIÓN, NODOS, TRABAJO
% :content:

# Annex VIII: Drop caches

Al final de cada trabajo de la partición thinnodes se ejecuta un drop
caches que limpia la cache de los nodos para evitar caídas del
rendimiento entre trabajos.
