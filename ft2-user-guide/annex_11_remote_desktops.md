(ft2_remote_desktops)=

% :title: Annex XI: Remote desktop on a job
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/annex_11_remote_desktops.html
% :topic: annex_11_remote_desktops
% :keywords: VIS, CESGA, REMOTE_DESKTOP, JOB, SIMULACIÓN, MONITORIZACIÓN, GRÁFICA, INTERACTIVA, MÓDULO, CARGA, SISTEMA_DE_COLAS, ESCRITORIO_REMOTO
% :content:

# Annex XI: Remote desktop on a job

Actualmente en todos los nodos es posible ejecutar un escritorio remoto
bajo un trabajo en el sistema de colas. Esto habilita la posibilidad de
monitorización directa de forma gráfica e interactiva de la simulación
en curso.

Esta opción se habilita mediante la carga del módulo:

**\$ module load vis/cesga**

Con el comando:

**\$ module help vis/cesga**

se obtiene una breve guía de uso.

{{ image23 }}
