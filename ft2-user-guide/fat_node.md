(ft2_fat_node)=

% :title: Fat node
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/fat_node.html
% :topic: fat_node
% :keywords: FAT NODE, CORES, MEMORIA, DISCO LOCAL, SCRATCH, MEMORIA COMPARTIDA, OPENMP, JAVA, THREADS, COLA DE TRABAJO, PARTICIONAMIENTO
% :content:

# Fat node

El Fat node es un único nodo (con nombre interno c6714) con 128 cores y
4TB de memoria principal, así como un disco local de scratch de 20TB. Es
especialmente útil en aplicaciones demandantes de grandes cantidades de
memoria y que paralelizan bien utilizando modelos basados en memoria
compartida (Threads, OpenMP ó Java). Para enviar trabajos a este nodo,
se debe añadir la opción **\*-partition fatnode\*** al enviar un trabajo
al sistema de colas.
