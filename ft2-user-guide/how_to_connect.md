(ft2_how_to_connect)=

% :title: How to connect
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/how_to_connect.html
% :topic: how_to_connect
% :keywords: SSH, LOGIN, NODES, CESGA, ACCOUNT, USERNAME, PASSWORD, CPU, MEMORY, VIRTUAL, MEMORY, JOB, EXECUTION, QUEUE, SYSTEM, INTERACTIVE, COMPUTE, COMMAND
% :content:

# How to connect

The connection to the Finis Terrae II login nodes must be made
via an ssh client, using the following address:

```
ssh username@ft2.cesga.es
```

Use your general CESGA account username and password.

Once connected to the system, you enter one of the login nodes,
from which you can edit and copy/move files, and
submit jobs to the queue system. These nodes are not intended for
job execution, and each session is CPU time-limited to 8
hours and 8GB of memory of virtual memory. For larger needs, a node should be used
interactive (using the compute command discussed ahead).
