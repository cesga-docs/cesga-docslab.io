(ft2_useful_options)=

% :title: Useful Options
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/useful_options.html
% :topic: useful_options
% :keywords: SQUEUE, SQSTAT, START, COLA, EQUIPO, UTILIZACIÓN, GLOBAL, ESPERA, TRABAJOS
% :content:

# Useful Options

**\*squeue --start\***: Muestra la hora estimada de comienzo de los
trabajos en espera

**\*sqstat:\*** Muestra información detallada de las colas y la
utilización global del equipo.
