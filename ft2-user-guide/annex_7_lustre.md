(ft2_lustre)=

% :title: Annex VII: Lustre
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/annex_7_lustre.html
% :topic: annex_7_lustre
% :keywords: LUSTRE, STRIPING, E/S, RENDIMIENTO, OSSS, OSTS, MPI-IO, HDF5, NETCDF, FINIS TERRAE II, SISTEMAS DE FICHEROS, ANCHO DE BANDA, OVERHEAD, RECOMENDACIÓN, TAMAÑO DEL FICHERO, CONFIGURACIÓN, STRIPE, LFS SETSTRIPE, LFS GETSTRIPE, COMANDO LS -L, CONTENCIÓN, DIRECTORIOS CON MUCHOS FICHEROS.
% :content:

# Annex VII: Lustre

Finis Terrae II utiliza Lustre para su directorio **\$LUSTRE_SCRATCH**.
En el caso de muchas aplicaciones, la utilización de striping de
ficheros puede mejorar el rendimiento de E/S a disco. Esta técnica
mejora especialmente el rendimiento de las aplicaciones que realizan E/S
serie desde un único servidor o que hace E/S paralela desde múltiples
servidores escribiendo en un único fichero compartido con MPI-IO, HDF5
paralelo o NetCDF paralelo.

El sistema de ficheros Lustre está formado por un conjunto de servidores
de E/S (OSSs) y de discos (OSTs). Un fichero se puede repartir entre
varios OSTs y entonces se denomina striping. De esta forma se mejora el
rendimiento de entrada salida al acceder a varios OSTs de forma
simultánea, incrementando el ancho de banda de E/S.

El striping por defecto en Finis Terrae II es de 1, lo que quiere decir
que por defecto sólo se utiliza 1 OST para cada fichero,
independientemente de su tamaño. El tamaño de un stripe es de 1MB.

Determinar el striping óptimo para un fichero no es fácil, ya que si se
realiza sobre pocos OSTs no sacará partido del ancho de banda
disponible, mientras que si se hace sobre demasiados, provocará un
overhead innecesario y pérdida de rendimiento. Para más información
pueden consultarse a [\*sistemas@cesga.es\*](mailto:sistemas@cesga.es).
La tabla siguiente muestra una recomendación sobre cómo configurar este
striping en función del tamaño del fichero:

```{eval-rst}
+----------------------+------------------------------------------------+------------------------------------------------+
| Tamaño del fichero   | E/S compartida entre todos los procesadores    | Un fichero por procesador                      |
+----------------------+------------------------------------------------+------------------------------------------------+
| <1GB                 | No hacer nada. Utilizar striping por defecto   | No hacer nada. Utilizar striping por defecto   |
+----------------------+------------------------------------------------+------------------------------------------------+
| 1GB-10GB             | 4 OSTs                                         | No hacer nada. Utilizar striping por defecto   |
+----------------------+------------------------------------------------+------------------------------------------------+
| 10GB-100GB           | 12 OSTs                                        | No hacer nada. Utilizar striping por defecto   |
+----------------------+------------------------------------------------+------------------------------------------------+
| 100GB-1TB            | 48 OSTs                                        | Preguntar                                      |
+----------------------+------------------------------------------------+------------------------------------------------+
```

El stripe de un fichero debe fijarse antes de empezar a escribir en él.
Por ejemplo, para crear el fichero “salida” que más adelante puede
crecer hasta 100GB, se utilizará el comando:

**\*lfs setstripe --count 12 salida\***

Que fijaría el stripesize del fichero salida en 12 OSTs

El mismo comando se puede utilizar para generar un directorio y
especificar el striping por defecto de todos los ficheros contenidos en
él. Por ejemplo, el siguiente comando crea el directorio dir_salida con
un striping de 12 para todos los ficheros que se generen dentro de él:

**\*lfs setstripe --count 12 dir_salida\***

El striping de un fichero no se puede cambiar una vez que se ha
comenzado a escribir en el fichero (a menos que se copia a un nuevo
fichero vacío creado con el striping deseado)

Para conocer el stripe de un fichero se puede utilizar el comando:

**\*lfs getstripe fichero\***

**IMPORTANTE: \*\*se debe minimizar o evitar la utilización del comando
“\*\*\*ls -l****”\*** en los directorios de Lustre, ya que provoca una
carga en el sistema. También debe evitarse la creación de directorios
con muchos ficheros, ya que cada vez que se abren varios ficheros de un
mismo directorio, se provoca una contención. En ese caso es preferible
tener varios subdirectorios con menos ficheros. También debe evitarse
almacenar los ficheros pequeños y ejecutables en Lustre, y mover esos
ficheros al directorio home o store.
