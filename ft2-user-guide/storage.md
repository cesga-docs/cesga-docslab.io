(ft2_storage)=

% :title: Storage
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/storage.html
% :topic: storage
% :keywords: FT2, STORAGE, HOME, STORE, LUSTRE, NFS, FILES, LIMIT, BACKUP, ENVIRONMENT, VARIABLE, USER, ACCESS, SPEED, CODE, SIMULATIONS, RESULTS, TEMPORAL, SCRATCH, DATA, BIG, FILES, PARALLEL, FILESYSTEM, TB, GB, MB, S
% :content:

# Storage

FT2 Home: \$HOME, 10GB and 100.000 files limit, NFS, 100MB/s

Store: \$STORE, 500GB and 300.000 files limit, NFS, 100MB/s

Lustre: \$LUSTRE, parallel file system, 3TB and 200.000 files limit, 20GB/s

```{eval-rst}
+--------------------+---------------------------------+----------------+----------+------------------------------------+
| Dir / Filesystem   | Use                             | User limits    | Backup   | Associated environment variable    |
+--------------------+---------------------------------+----------------+----------+------------------------------------+
| Home               | Store code files                | 10GB           | Yes      | $HOME                              |
|                    |                                 |                |          |                                    |
|                    | Low speed access                | 100.000 files  |          |                                    |
+--------------------+---------------------------------+----------------+----------+------------------------------------+
| Store              | Store simulations final results | 100GB          | No       | $STORE                             |
|                    |                                 |                |          |                                    |
|                    | Low speed access                | 300.000 files  |          |                                    |
+--------------------+---------------------------------+----------------+----------+------------------------------------+
| Lustre             | Temporal and scratch data       | 3TB            | No       | $LUSTRE                            |
|                    |                                 |                |          |                                    |
|                    | Big files oriented              |                |          |                                    |
|                    |                                 |                |          |                                    |
|                    | High speed access               | 200.000 files  |          |                                    |
+--------------------+---------------------------------+----------------+----------+------------------------------------+
```
