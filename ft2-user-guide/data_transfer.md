(ft2_data_transfer)=

% :title: Data transfer
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/data_transfer.html
% :topic: data_transfer
% :keywords: SCP, SFTP, FINIS TERRAE II, DATA TRANSFER
% :content:

# Data transfer

For files transfer to or from the Finis Terrae II an scp or sftp client must be used.
