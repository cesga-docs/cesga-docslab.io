(ft2_gpu_nodes)=

% :title: GPU nodes
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/gpu_nodes.html
% :topic: gpu_nodes
% :keywords: GPU, SBATCH, GRES, GPU2, HELP, ENFORCE-BINDING, CPUS, GRES, GRES.CONF, NODE, PARTITION, FINIS TERRAE II, GPU-SHARED-V100, SRUN, NVIDIA-SMI, TOPO, CUDA, CAPABILITY, MEMORY, BANDWIDTH, MULTIPROCESSORS, CORES, CLOCK RATE, COMPUTE, QOS, SHARED
% :content:

# GPU nodes

Para el uso de cualquier GPU al enviar trabajos es necesario indicar las
opciones **\*--gres=gpu\*** (si no se indica la opción **\*--gres=gpu\*** no
se asignará la GPU). En la página del manual de sbatch aparece:

> **--gres=\<list>**
>
> Specifies a comma delimited list of generic consumable resources. The

format of each entry on the list is "name\[\[:type\]:count\]". The name is
that of the consumable resource. The count is the number of those
resources with a default value of 1. The specified resources will be
allocated to the job on each node. The available generic consumable
resources is configurable by the system administrator. A list of
available generic consumable resources will be printed and the command
will exit if the option argument is "help". Examples of use include
"--gres=gpu", "--gres=gpu2", and "--gres=help".

> **--gres-flags=enforce-binding**
>
> If set, the only CPUs available to the job will be those bound to the

selected GRES (i.e. the CPUs identified in the gres.conf file will be
strictly enforced rather than advisory). This option may result in
delayed initiation of a job. For example a job requiring two GPUs and
one CPU will be delayed until both GPUs on a single socket are available
rather than using GPUs bound to separate sockets, however the
application performance may be improved due to improved communication
speed. Requires the node to be configured with more than one socket and
resource filtering will be performed on a per-socket basis.

Adicionalmente es necesario enviar al trabajo a la correspondiente
partición según el modelo de GPU.

Existen los siguientes modelos de GPU actualmente habilitados en el
Finis Terrae II:

```{eval-rst}
+--------------------------------------------------------------------------------+
+--------------------------------------------------------------------------------+
| ***--partition=gpu-shared-v100***                                              |
+--------------------------------------------------------------------------------+
| ***srun -p gpu-shared-v100 --gres=gpu -t 20 nvidia-smi topo -m***              |
|                                                                                |
| |image14|                                                                      |
+--------------------------------------------------------------------------------+
| Per GPU:                                                                       |
| CUDA Capability Major/Minor version number:3.7                                 |
| **MEMORY: 12 GB** of GDDR5 BANDWIDTH 240 GB/s                                  |
| (13) Multiprocessors, (192) CUDA Cores/MP: 2496 CUDA Cores                     |
| GPU Max Clock rate: 824 MHz (0.82 GHz)                                         |
+--------------------------------------------------------------------------------+
| Uso interactivo: ***compute --gpu***                                           |
|                                                                                |
| ***--partition=gpu-shared***                                                   |
+--------------------------------------------------------------------------------+
| ***srun -p gpu-shared --gres=gpu -t 20 nvidia-smi topo -m***                   |
|                                                                                |
| |image15|                                                                      |
+--------------------------------------------------------------------------------+
+--------------------------------------------------------------------------------+
| ***--partition=gpu-shared-k2 --qos=shared***                                   |
+--------------------------------------------------------------------------------+
| ***srun -p gpu-shared-k2 --qos=shared --gres=gpu -t 20 nvidia-smi topo -m***   |
|                                                                                |
| |image16|                                                                      |
+--------------------------------------------------------------------------------+
+--------------------------------------------------------------------------------+
| Uso interactivo: ***compute --gpu-t4***                                        |
+--------------------------------------------------------------------------------+
| ***nvidia-smi topo -m***                                                       |
|                                                                                |
| |image17|                                                                      |
+--------------------------------------------------------------------------------+
```
