(ft2_overview)=

% :title: Overview
% :section: ft2-user-guide
% :url_source: https://cesga-docs.gitlab.io/ft2-user-guide/overview.html
% :topic: overview
% :keywords: HASWELL, INFINIBAND, INTEL, CLUSTER, PERFORMANCE, LINPACK, TFLOPS
% :content:

# Overview

Finis Terrae II is a computer cluster system based on processors
Intel Haswell and interconnected via an Infiniband network with a
peak performance of 328 TFlops and sustained Linpack performance of 213 Tflops. Specific
details about its configuration in Annex I.
